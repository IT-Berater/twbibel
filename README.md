# TWBibel

[![pipeline status](https://gitlab.com/IT-Berater/twbibel/badges/master/pipeline.svg)](https://gitlab.com/IT-Berater/twbibel/commits/master)

Details zum Generator siehe auf dem Blog [www.wenzlaff.de](http://blog.wenzlaff.de/?p=13590) und http://blog.wenzlaff.de/?s=twbibel

Und auch aktuell unter [diesen Eintrag]( http://blog.wenzlaff.de/?p=18908).


Java Lib (für Java 19) für Bibel Funktionen und BE.

Folgende BEs werden zur Verfügung gestellt:

* Bibel (class)
* Bibelbuch (class)
* Bibelbuecher (enum)

Einfach per Maven:

~~~
<dependency>
  <groupId>de.wenzlaff.twbibel</groupId>
  <artifactId>de.wenzlaff.twbibel</artifactId>
  <version>0.0.5</version>
</dependency>
~~~

einbinden. Oder per Gradle


    implementation 'de.wenzlaff.twbibel:de.wenzlaff.twbibel:0.0.5'


In dieser UML-Übersicht ist das ganze BE zu sehen:


![](uml/architektur.png)

Und für den Generator den Kommandozeilen-Client:

![](uml/generator.png)

CMD:
java -cp de.wenzlaff.twbibel-0.1.0-jar-with-dependencies.jar de.wenzlaff.bibelleseplan.Kommandozeile -h

Mit JavaFX:
java --module-path $PATH_TO_FX --add-modules ALL-MODULE-PATH -jar de.wenzlaff.twbibel-0.1.0-jar-with-dependencies.jar -h
