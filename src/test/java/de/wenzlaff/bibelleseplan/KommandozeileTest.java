package de.wenzlaff.bibelleseplan;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import de.wenzlaff.twbibel.Bibelbuecher;

/**
 * Testklasse für den Kommandozeilen Client.
 * 
 * @author Thomas Wenzlaff
 *
 */
public class KommandozeileTest {

	private static final String TEST_DATUM_AB = "2025-01-01";

	/**
	 * Testet das erzeugen aller Bibelbücher als einzelne Datei.
	 * 
	 * @throws Exception
	 */
	@Disabled
	@Test
	void testStartMulitBuecherl() {

		for (Bibelbuecher buch : Bibelbuecher.values()) {
			System.out.println(buch);
			String[] args = { "-s", TEST_DATUM_AB, "-b", buch.toString() };
			try {
				Kommandozeile.main(args);
				Thread.sleep(50);
			} catch (Exception e) {
				System.err.println("Buch: " + buch.toString() + e.getLocalizedMessage());
			}
		}
	}

	/**
	 * Testet das Erzeugen eines Bibelbuches: Koloser das war in der Version 0.0.5
	 * nicht vorhanden.
	 * 
	 * @throws Exception
	 */
	@Test
	void testStartDatumBibelbuchKolosser() throws Exception {

		String[] args = { "-s", TEST_DATUM_AB, "-b", "Kolosser" };
		Kommandozeile.main(args);
	}

	/**
	 * Erzeugt alle Bibeln für den Blog und das neue Jahr.
	 * 
	 * @throws Exception
	 */
	@Test
	void pläneFürDenBlogErzeugen() throws Exception {

		String[] args1 = { "-s", TEST_DATUM_AB, "-u", "NWT" };
		Kommandozeile.main(args1);

		String[] args2 = { "-s", TEST_DATUM_AB, "-u", "RBI" };
		Kommandozeile.main(args2);

		String[] args3 = { "-s", TEST_DATUM_AB, "-u", "BI12" };
		Kommandozeile.main(args3);
	}

	/**
	 * Testet das erzeugen einer ganzen Bibel mit aktuellen Datum.
	 * 
	 * @throws Exception
	 */
	@Test
	void testStartGanzeBibelDefaultDatumNWT() throws Exception {
		// Parameter:
		// Startdatum nicht angegeben
		// Bibelbuch Name nicht angegeben, ganze Bibel
		String[] args = { "" };
		Kommandozeile.main(args);
	}

	/**
	 * Testet das erzeugen einer ganzen Bibel (RBI) Studienbibel mit aktuellen
	 * Datum.
	 * 
	 * @throws Exception
	 */
	@Test
	void testStartGanzeBibelDefaultDatumStudienbibelRBI() throws Exception {
		// Parameter:
		// Startdatum nicht angegeben
		// Bibelbuch Name nicht angegeben, ganze Bibel
		String[] args = { "-u", "RBI" };
		Kommandozeile.main(args);
	}

	/**
	 * Testet das erzeugen einer ganzen Bibel NWT mit aktuellen Datum.
	 * 
	 * @throws Exception
	 */
	@Test
	void testStartGanzeBibelDefaultDatumStudienbibelNWT() throws Exception {
		// Parameter:
		// Startdatum nicht angegeben
		// Bibelbuch Name nicht angegeben, ganze Bibel
		String[] args = { "-u", "NWT" };
		Kommandozeile.main(args);
	}

	/**
	 * Testet das erzeugen einer ganzen Bibel BI12 mit aktuellen Datum.
	 * 
	 * @throws Exception
	 */
	@Test
	void testStartGanzeBibelDefaultDatumStudienbibelBI12() throws Exception {
		// Parameter:
		// Startdatum nicht angegeben
		// Bibelbuch Name nicht angegeben, ganze Bibel
		String[] args = { "-u", "BI12" };
		Kommandozeile.main(args);
	}

	/**
	 * Testet das erzeugen einer ganzen Bibel.
	 * 
	 * @throws Exception
	 */
	@Test
	void testStartGanzeBibel() throws Exception {
		// Parameter:
		// Startdatum im Format JJJJ-MM-TT,
		// Bibelbuch Name (Optional) wenn nicht
		// angegeben, ganze Bibel
		String[] args = { "-s", TEST_DATUM_AB };
		Kommandozeile.main(args);
	}

	/**
	 * Testet das Erzeugen eines Bibelbuches: Jona
	 * 
	 * @throws Exception
	 */
	@Test
	void testStartDatumBibelbuch() throws Exception {
		// Parameter:
		// Startdatum im Format JJJJ-MM-TT,
		// Bibelbuch Name (Optional) wenn nicht
		// angegeben, ganze Bibel
		String[] args = { "-s", TEST_DATUM_AB, "-b", "Jona" };
		Kommandozeile.main(args);
	}

	/**
	 * Testet das Erzeugen eines Bibelbuches: Matthäus
	 * 
	 * @throws Exception
	 */
	@Test
	void testStartDatumBibelbuch2() throws Exception {
		// Parameter:
		// Startdatum im Format JJJJ-MM-TT,
		// Bibelbuch Name (Optional) wenn nicht
		// angegeben, ganze Bibel
		String[] args = { "-s", TEST_DATUM_AB, "-b", "Matthäus" };
		Kommandozeile.main(args);
	}

	/**
	 * Testet das Erzeugen eines Bibelbuches: 1. Mose
	 * 
	 * @throws Exception
	 */
	@Test
	void testStartDatumBibelbuch1Mose() throws Exception {
		// Parameter:
		// Startdatum im Format JJJJ-MM-TT,
		// Bibelbuch Name (Optional) wenn nicht
		// angegeben, ganze Bibel
		String[] args = { "-s", TEST_DATUM_AB, "-b", "1. Mose" };
		Kommandozeile.main(args);
	}

	/**
	 * Testet das Erzeugen eines Bibelbuches mit ungültigen Namen.
	 * 
	 * @throws Exception
	 */
	@Test
	void testStartDatumBibelbuchNeg() throws Exception {
		// Parameter:
		// Startdatum im Format JJJJ-MM-TT,
		// Bibelbuch Name (Optional) wenn nicht
		// angegeben, ganze Bibel
		String[] args = { "-s", TEST_DATUM_AB, "-b", "Thomas" };
		Kommandozeile.main(args);
	}

	/**
	 * Testet die Hilfefunktion.
	 * 
	 * @throws Exception
	 */
	@Test
	void testHilfeBibelbuch() throws Exception {
		// Parameter:
		// Hilfe
		String[] args = { "-h" };
		Kommandozeile.main(args);
	}
}
