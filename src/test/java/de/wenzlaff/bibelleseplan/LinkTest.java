package de.wenzlaff.bibelleseplan;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import de.wenzlaff.twbibel.Bibelbuecher;
import de.wenzlaff.twbibel.JwOrg;

/**
 * Test für die Links.
 * 
 * @author Thomas Wenzlaff
 *
 */
class LinkTest {

	private static final Logger LOG = LogManager.getLogger(LinkTest.class);

	@Test
	void testGetBibelserverComUrlKapitelVers() {

		Link l = new Link();
		LOG.info(l.getBibelserverComUrl("ELB", "Psalm83,19"));
	}

	@Test
	void testGetBibelserverComUrlNurKapitel() {

		Link l = new Link();
		LOG.info(l.getBibelserverComUrl("ELB", "Psalm83"));
	}

	@Test
	void testURLl() throws Exception {
		Parameter parameter = new Parameter();
		parameter.setSprache("de");
		parameter.setBibelübersetzung("RBI");
		parameter.setBibelbuch(Bibelbuecher.DRITTER_JOHANNES);
		LOG.info(JwOrg.getOnlineBibelLink(parameter, Integer.valueOf("1")));
	}
}
