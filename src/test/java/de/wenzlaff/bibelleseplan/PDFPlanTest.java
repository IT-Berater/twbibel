package de.wenzlaff.bibelleseplan;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import picocli.CommandLine;

/**
 * Test PDF Plan.
 * 
 * @author Thomas Wenzlaff
 *
 */
public class PDFPlanTest {

	@Test
	void testErzeuge5KapitelProTagAb() {
		PDFPlan app = new PDFPlan();
		CommandLine cmd = new CommandLine(app);

		int exitCode = cmd.execute("-k=5", "-s=01.01.2021");
		assertEquals(0, exitCode);
	}

	@Test
	void testFalschesDatum() {
		PDFPlan app = new PDFPlan();
		CommandLine cmd = new CommandLine(app);

		int exitCode = cmd.execute("-k=5", "-s=01012021");
		assertEquals(2, exitCode);
	}
}
