package de.wenzlaff.bibelleseplan;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;

import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.TimeZone;

/**
 * Test des Kalenders.
 * 
 * @author Thomas Wenzlaff
 *
 */
class KalenderTest {

	@Test
	void testKalender() {
		Kalender k = new Kalender();
		assertNotNull(k);
	}

	@Test
	void testAddTermin() {
		Kalender k = new Kalender();

		Calendar icsCalendar = Kalender.createKalender();
		TimeZone timezone = Kalender.createTimezoneEuropa();
		LocalDateTime eventStartZeitpunkt = LocalDateTime.now();

		String eventKommentar = "JUnit Test Kommentar";

		String eventTitel = "JUnit Event Titel";
		k.addTermin(icsCalendar, timezone, eventStartZeitpunkt, eventTitel, eventKommentar);
	}

	@Test
	void testCreateKalender() {

		Calendar icsCalendar = Kalender.createKalender();
		assertNotNull(icsCalendar);
	}

	@Test
	void testCreateTimezoneEuropa() {
		TimeZone timezone = Kalender.createTimezoneEuropa();
		assertNotNull(timezone);
	}

	@Test
	void testWriteKalenderInDatei() throws Exception {

		Calendar icsCalendar = getTestkalenderMitEinTermin();

		String icsDateinamen = "JUnit-Testdatei.ics";
		Kalender.writeKalenderInDatei(icsCalendar, icsDateinamen);
	}

	private Calendar getTestkalenderMitEinTermin() {
		Calendar icsCalendar = Kalender.createKalender();
		TimeZone timezone = Kalender.createTimezoneEuropa();
		LocalDateTime eventStartZeitpunkt = LocalDateTime.now();
		String eventKommentar = "JUnit Test Kommentar";
		String eventTitel = "JUnit Event Titel";
		Kalender k = new Kalender();
		k.addTermin(icsCalendar, timezone, eventStartZeitpunkt, eventTitel, eventKommentar);
		return icsCalendar;
	}

}
