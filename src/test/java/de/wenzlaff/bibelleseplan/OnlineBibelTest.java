package de.wenzlaff.bibelleseplan;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

import de.wenzlaff.twbibel.BibelUebersetzungen;
import de.wenzlaff.twbibel.Bibelbuecher;
import de.wenzlaff.twbibel.BibelserverCom;
import de.wenzlaff.twbibel.JwOrg;
import de.wenzlaff.twbibel.Uebersetzung;

/**
 * Check Online-Bibel Links.
 * 
 * @author Thomas Wenzlaff
 *
 */
class OnlineBibelTest {

	private static final String TEST_ABK = "LUT, ELB, HFA, SLT, ZB, NGÜ, GNB, EU, ESV, NLB, NIV, NeÜ, MENG, NIRV, KJV, BDS, S21, ITA, NRS, HTB, LSG, CST, NVI, BTX, PRT, NOR, BSV, DK, PSZ, CEP, SNC, B21, BKR, NPK, KAR, HUN, NTR, BGV, CBT, CKK, RSZ, CARS, TR, NAV, FCB, CUVS, CCBT, OT, LXX, RBI, NWT";

	/**
	 * Sortiert, nach alph. aber jede Liste angehangen.
	 */
	private static final String TEST_ABK_SORTIERT = "B21, BDS, BGV, BKR, BSV, BTX, CARS, CBT, CCBT, CEP, CKK, CST, CUVS, DK, ELB, ESV, EU, FCB, GNB, HFA, HTB, HUN, ITA, KAR, KJV, LSG, LUT, LXX, MENG, NAV, NGÜ, NIRV, NIV, NLB, NOR, NPK, NRS, NTR, NVI, NeÜ, OT, PRT, PSZ, RSZ, S21, SLT, SNC, TR, ZB, BI12, NWT, RBI";

	private static final String NWT_LINK = "https://www.jw.org/de/bibliothek/bibel/nwt/bibelbuecher/";

	/**
	 * Test der URL.
	 */
	@Test
	void testOnlineBibelEinfacherNamen() {

		Parameter parameter = getTestParameterNwt();
		parameter.setBibelbuch(Bibelbuecher.JOEL.toString());

		BibelUebersetzungen o = new JwOrg(parameter, 1);

		assertEquals(NWT_LINK + "Joel/1/", o.getOnlineBibelURL().toString());
	}

	/**
	 * Test der URL.
	 */
	@Test
	void testOnlineBibelMitZweiten() {

		Parameter parameter = getTestParameterNwt();
		parameter.setBibelbuch(Bibelbuecher.ZWEITER_SAMUEL.toString());

		BibelUebersetzungen o = new JwOrg(parameter, 1);

		assertEquals(NWT_LINK + "2-samuel/1/", o.getOnlineBibelURL().toString());
	}

	/**
	 * Test der URL.
	 */
	@Test
	void testOnlineBibelMitUmlauten() {

		Parameter parameter = getTestParameterNwt();
		parameter.setBibelbuch(Bibelbuecher.HEBRÄER.toString());

		BibelUebersetzungen o = new JwOrg(parameter, 1);

		assertEquals(NWT_LINK + "Hebräer/1/", o.getOnlineBibelURL().toString());
	}

	/**
	 * Test der URL.
	 */
	@Test
	void testOnlineBibelMitUmlauten2() {

		Parameter parameter = getTestParameterNwt();
		parameter.setBibelbuch(Bibelbuecher.SPRÜCHE.toString());

		BibelUebersetzungen o = new JwOrg(parameter, 5);

		assertEquals(NWT_LINK + "Sprüche/5/", o.getOnlineBibelURL().toString());
	}

	/**
	 * Test der URL.
	 */
	@Test
	void testOnlineBibelMitSonderfallZwei() {

		Parameter parameter = getTestParameterNwt();

		parameter.setBibelbuch(Bibelbuecher.HOHESLIED.toString());

		BibelUebersetzungen o = new JwOrg(parameter, 1);

		assertEquals(NWT_LINK + "hohes-lied/1/", o.getOnlineBibelURL().toString());
	}

	@Test
	void testOnlineBibelUngueltigesKapitel() {

		Parameter parameter = getTestParameterNwt();

		parameter.setBibelbuch(Bibelbuecher.HOHESLIED.toString());

		BibelUebersetzungen o = new JwOrg(parameter, 89);

		assertEquals(NWT_LINK + "hohes-lied/8/", o.getOnlineBibelURL().toString());
	}

	private Parameter getTestParameterNwt() {
		Parameter parameter = new Parameter();
		parameter.setSprache("de");
		parameter.setBibelübersetzung("NWT");
		return parameter;
	}

	/**
	 * Test der URL.
	 */
	@Test
	void testOnlineBibelUngueltigesKapitelZuKlein() {

		Parameter parameter = getTestParameterNwt();
		parameter.setBibelbuch(Bibelbuecher.HOHESLIED.toString());

		BibelUebersetzungen o = new JwOrg(parameter, 0);

		assertEquals(NWT_LINK + "hohes-lied/1/", o.getOnlineBibelURL().toString());
	}

	/**
	 * Liefert alle Möglichen Abkürzungen.
	 */
	@Test
	void testAlleMoeglichenAbkuerzungen() {

		BibelserverCom b = new BibelserverCom();

		List<Uebersetzung> alle = b.getUebersetzungen();
		assertEquals(49, alle.size());

		Parameter parameter = getTestParameterNwt();
		parameter.setBibelbuch(Bibelbuecher.AMOS);

		JwOrg jw = new JwOrg(parameter, 1);
		List<Uebersetzung> jwAlle = jw.getUebersetzungen();
		assertEquals(3, jwAlle.size());

		assertEquals(TEST_ABK_SORTIERT, BibelUebersetzungen.getAllAbk(alle, jwAlle));
	}

	/**
	 * Liefert alle Möglichen Abkürzungen.
	 */
	@Test
	void testAlleMoeglichenAbkuerzungenHilfe() {

		assertEquals(259, BibelUebersetzungen.getAllAbkSortiert().length(), "Da ist wohl eine Übersetzung hinzu gekommen.");
		System.out.println(BibelUebersetzungen.getAllAbkSortiert());
	}
}