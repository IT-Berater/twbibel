package de.wenzlaff.twbibel;

import org.apache.logging.log4j.Logger;

import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.fields;

/**
 * Architektur vorgaben.
 * 
 * Siehe https://www.archunit.org/userguide/html/000_Index.html
 * 
 * @author Thomas Wenzlaff
 */
@AnalyzeClasses(packages = "de.wenzlaff")
public class ArchitekturValidator {

	/**
	 * Apache Log4J Logger nur private, final und static und Feldname LOG
	 */
	@ArchTest
	static final ArchRule Logger_Regel = fields().that()
			.haveRawType(Logger.class).should().bePrivate().andShould()
			.beStatic().andShould().beFinal().andShould().haveName("LOG")
			.because("Eigene Architektur Regel");
}