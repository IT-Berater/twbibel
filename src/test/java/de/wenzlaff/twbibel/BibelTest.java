package de.wenzlaff.twbibel;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

/**
 * Testklasse für die Bibel.
 * 
 * @author Thomas Wenzlaff
 *
 */
public class BibelTest {

	@Test
	void testMaxKapitel() {

		assertEquals(1189, Bibel.maxKapitel());
	}

	@Test
	void testGetBibel() {

		// Bibel.getBibel().forEach(buch -> System.out.println(buch));

		assertEquals(66, Bibel.getBibel().size());
	}

	@Test
	void testIsATl() {

		assertEquals(true, Bibel.getBibel().get(Bibelbuecher.MATTHÄUS.ordinal()).isNT());
		assertEquals(false, Bibel.getBibel().get(Bibelbuecher.MATTHÄUS.ordinal()).isAT());
	}

	/**
	 * Um die Bibel in einem Jahr zu lesen, reicht es 3 Kapitel in der Woche und am
	 * Wochenende 4 Kapitel.
	 */
	@Test
	void testGetJedesKapitelInBibel() {

		// Bibel.getAlleKapitel().stream().forEach(buch -> System.out.println(buch));

		assertEquals(1189, Bibel.getAlleKapitel().size());
	}

	/**
	 * Um die Bibel in einem Jahr zu lesen, reicht es 3 Kapitel in der Woche und am
	 * Wochenende 4 Kapitel.
	 */
	@Test
	void testEinBibelBuch() {

		List<String> fuenfterMose = Bibel.getAlleKapitel(Bibelbuecher.VIERTER_MOSE);

		assertEquals(36, fuenfterMose.size());

//		for (String kap : fuenfterMose) {
//			System.out.println(kap);
//		}
	}

	/**
	 * Check der get Schreiber Methoden.
	 */
	@Test
	public void testGetSchreiber() {
		assertEquals("Amos", Bibel.gebBibelschreiber(Bibelbuecher.AMOS));
		assertEquals("Amos", Bibelbuecher.AMOS.getBibelschreiber());

		assertEquals("Moses", Bibel.gebBibelschreiber(Bibelbuecher.ERSTER_MOSE));
		assertEquals("Moses", Bibelbuecher.ERSTER_MOSE.getBibelschreiber());
	}
}
