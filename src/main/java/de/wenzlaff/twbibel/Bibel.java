package de.wenzlaff.twbibel;

import java.util.ArrayList;
import java.util.List;

/**
 * Die Online-Bibel ldt. jw.org
 * 
 * @author Thomas Wenzlaff
 */
public final class Bibel {

	/** Trenner zwischen Biblebuch und Kapitel. */
	public static final String TRENNER = " Kapitel ";

	private static final List<Bibelbuch> bibel = new ArrayList<>();

	static {
		// Nr, Name, max. Kap., SCHREIBER, WO GESCHRIEBEN, WANN FERTIG (V. U. +  Z.)
		// ,ZEITSPANNE (V. U. Z.)
		bibel.add(new Bibelbuch(1, "1. Mose", 50, Bibelbuecher.ERSTER_MOSE, "Moses", "Wildnis", "1513", "Am Anfang bis 1657"));
		bibel.add(new Bibelbuch(2, "2. Mose", 40, Bibelbuecher.ZWEITER_MOSE, "Moses", "Wildnis", "1512", "1657–1512"));
		bibel.add(new Bibelbuch(3, "3. Mose", 27, Bibelbuecher.DRITTER_MOSE, "Moses", "Wildnis", "1512", "1 Monat (1512)"));
		bibel.add(new Bibelbuch(4, "4. Mose", 36, Bibelbuecher.VIERTER_MOSE, "Moses", "Wildnis und Ebenen Moabs", "1473", "1512–1473"));
		bibel.add(new Bibelbuch(5, "5. Mose", 34, Bibelbuecher.FUENFTER_MOSE, "Moses", "Ebenen Moabs", "1473", "2 Monate (1473)"));
		bibel.add(new Bibelbuch(6, "Josua", 24, Bibelbuecher.JOSUA, "Josua", "Kanaan", "um 1450", "1473 bis um 1450"));
		bibel.add(new Bibelbuch(7, "Richter", 21, Bibelbuecher.RICHTER, "Samuel", "Israel", "um 1100", "um 1450 bis um 1120"));
		bibel.add(new Bibelbuch(8, "Ruth", 4, Bibelbuecher.RUTH, "Samuel", "Israel", "um 1090", "11 Jahre Richterzeit"));
		bibel.add(new Bibelbuch(9, "1. Samuel", 31, Bibelbuecher.ERSTER_SAMUEL, "Samuel; Gad; Nathan", "Israel", "um 1078", "um 1180–1078"));
		bibel.add(new Bibelbuch(10, "2. Samuel", 24, Bibelbuecher.ZWEITER_SAMUEL, "Gad; Nathan", "Israel", "um 1040", "1077 bis um 1040"));
		bibel.add(new Bibelbuch(11, "1. Könige", 22, Bibelbuecher.ERSTER_KÖNIGE, "Jeremia", "Juda", "580", "um 1040–911"));
		bibel.add(new Bibelbuch(12, "2. Könige", 25, Bibelbuecher.ZWEITER_KÖNIGE, "Jeremia", "Juda und Ägypten", "580", "um 920–580"));
		bibel.add(new Bibelbuch(13, "1. Chronika", 29, Bibelbuecher.ERSTER_CHRONIKA, "Esra", "Jerusalem (?)", "um 460", "Nach 1. Chronika 9:44: um 1077–1037"));
		bibel.add(new Bibelbuch(14, "2. Chronika", 36, Bibelbuecher.ZWEITER_CHRONIKA, "Esra", "Jerusalem (?)", "um 460", "um 1037–537"));
		bibel.add(new Bibelbuch(15, "Esra", 10, Bibelbuecher.ESRA, "Esra", "Jerusalem", "um 460", "537 bis um 467"));
		bibel.add(new Bibelbuch(16, "Nehemia", 13, Bibelbuecher.NEHEMIA, "Nehemia", "Jerusalem", "n. 443", "456 bis n. 443"));
		bibel.add(new Bibelbuch(17, "Esther", 10, Bibelbuecher.ESTHER, "Mordechai", "Susa (Elam)", "um 475", "493 bis um 475 "));
		bibel.add(new Bibelbuch(18, "Hiob", 42, Bibelbuecher.HIOB, "Moses", "Wildnis", "um 1473", "Über 140 Jahre zwischen 1657 und 1473"));
		bibel.add(new Bibelbuch(19, "Psalmen", 150, Bibelbuecher.PSALMEN, "David und andere", "", "um 460", ""));
		bibel.add(new Bibelbuch(20, "Sprüche", 31, Bibelbuecher.SPRÜCHE, "Salomo; Agur; Lemuel", "Jerusalem", "um 717", ""));
		bibel.add(new Bibelbuch(21, "Prediger", 12, Bibelbuecher.PREDIGER, "Salomo", "Jerusalem", "v. 1000", ""));
		bibel.add(new Bibelbuch(22, "Hohes Lied", 8, Bibelbuecher.HOHESLIED, "Salomo", "Jerusalem", "um 1020", ""));
		bibel.add(new Bibelbuch(23, "Jesaja", 66, Bibelbuecher.JESAJA, "Jesaja", "Jerusalem", "n. 732", "um 778 bis n. 732"));
		bibel.add(new Bibelbuch(24, "Jeremia", 52, Bibelbuecher.JEREMIA, "Jeremia", "Juda; Ägypten", "580", "647–580"));
		bibel.add(new Bibelbuch(25, "Klagelieder", 5, Bibelbuecher.KLAGELIEDER, "Jeremia", "bei Jerusalem", "607", ""));
		bibel.add(new Bibelbuch(26, "Hesekiel", 48, Bibelbuecher.HESEKIEL, "Hesekiel", "Babylon", "um 591", "613 bis um 591"));
		bibel.add(new Bibelbuch(27, "Daniel", 12, Bibelbuecher.DANIEL, "Daniel", "Babylon", "um 536", "618 bis um 536"));
		bibel.add(new Bibelbuch(28, "Hosea", 14, Bibelbuecher.HOSEA, "Hosea", "Samaria (Bezirk)", "n. 745", "v. 804 bis n. 745"));
		bibel.add(new Bibelbuch(29, "Joel", 3, Bibelbuecher.JOEL, "Joel", "Juda", "um 820 (?)", ""));
		bibel.add(new Bibelbuch(30, "Amos", 9, Bibelbuecher.AMOS, "Amos", "Juda", "um 804", ""));
		bibel.add(new Bibelbuch(31, "Obadja", 1, Bibelbuecher.OBADJA, "Obadja", "", "um 607", ""));
		bibel.add(new Bibelbuch(32, "Jona", 4, Bibelbuecher.JONA, "Jona", "", "um 844", ""));
		bibel.add(new Bibelbuch(33, "Micha", 7, Bibelbuecher.MICHA, "Micha", "Juda", "v. 717", "um 777–717"));
		bibel.add(new Bibelbuch(34, "Nahum", 3, Bibelbuecher.NAHUM, "Nahum", "Juda", "v. 632", ""));
		bibel.add(new Bibelbuch(35, "Habakuk", 3, Bibelbuecher.HABAKUK, "Habakuk", "Juda", "um 628 (?)", ""));
		bibel.add(new Bibelbuch(36, "Zephanja", 3, Bibelbuecher.ZEPHANJA, "Zephanja", "Juda", "v. 648", ""));
		bibel.add(new Bibelbuch(37, "Haggai", 2, Bibelbuecher.HAGGAI, "Haggai", "wieder aufgebautes Jerusalem", "520", "112 Tage (520)"));
		bibel.add(new Bibelbuch(38, "Sacharja", 14, Bibelbuecher.SACHARJA, "Sacharja", "wieder aufgebautes Jerusalem", "518", "520–518"));
		bibel.add(new Bibelbuch(39, "Maleachi", 4, Bibelbuecher.MALEACHI, "Maleachi", "wieder aufgebautes Jerusalem", "n. 443", ""));
		// NT
		bibel.add(new Bibelbuch(40, "Matthäus", 28, Bibelbuecher.MATTHÄUS, "Matthäus", "Israel", "um 41", "2 v. u. Z. bis 33 u. Z."));
		bibel.add(new Bibelbuch(41, "Markus", 16, Bibelbuecher.MARKUS, "Markus", "Rom", "um 60–65", "29–33 u. Z."));
		bibel.add(new Bibelbuch(42, "Lukas", 24, Bibelbuecher.LUKAS, "Lukas", "Cäsarea", "um 56–58", "3 v. u. Z. bis 33 u. Z."));
		bibel.add(
				new Bibelbuch(43, "Johannes", 21, Bibelbuecher.JOHANNES, "Apostel Johannes", "Ephesus oder in der Nähe", "um 98", "Nach Vorrede: 29–33 u. Z."));
		bibel.add(new Bibelbuch(44, "Apostelgeschichte", 28, Bibelbuecher.APOSTELGESCHICHTE, "Lukas", "Rom", "um 61", "33 bis um 61 u. Z."));
		bibel.add(new Bibelbuch(45, "Römer", 16, Bibelbuecher.RÖMER, "Paulus", "Korinth", "um 56", ""));
		bibel.add(new Bibelbuch(46, "1. Korinther", 16, Bibelbuecher.ERSTER_KORINTHER, "Paulus", "Ephesus", "um 55", ""));
		bibel.add(new Bibelbuch(47, "2. Korinther", 13, Bibelbuecher.ZWEITER_KORINTHER, "Paulus", "Mazedonien", "um 55", ""));
		bibel.add(new Bibelbuch(48, "Galater", 6, Bibelbuecher.GALATER, "Paulus", "Korinth oder syrisches Antiochia", "um 50–52", ""));
		bibel.add(new Bibelbuch(49, "Epheser", 6, Bibelbuecher.EPHESER, "Paulus", "Rom", "um 60/61", ""));
		bibel.add(new Bibelbuch(50, "Philipper", 4, Bibelbuecher.PHILIPPER, "Paulus", "Rom", "um 60/61", ""));
		bibel.add(new Bibelbuch(51, "Kolosser", 4, Bibelbuecher.KOLOSSER, "Paulus", "Rom", "um 60/61", ""));
		bibel.add(new Bibelbuch(52, "1. Thessalonicher", 5, Bibelbuecher.ERSTER_THESSALONICHER, "Paulus", "Korinth", "um 50", ""));
		bibel.add(new Bibelbuch(53, "2. Thessalonicher", 3, Bibelbuecher.ZWEITER_THESSALONICHER, "Paulus", "Korinth", "um 51", ""));
		bibel.add(new Bibelbuch(54, "1. Timotheus", 6, Bibelbuecher.ERSTER_TIMOTHEUS, "Paulus", "Mazedonien", "um 61–64", ""));
		bibel.add(new Bibelbuch(55, "2. Timotheus", 4, Bibelbuecher.ZWEITER_TIMOTHEUS, "Paulus", "Rom", "um 65", ""));
		bibel.add(new Bibelbuch(56, "Titus", 3, Bibelbuecher.TITUS, "Paulus", "Mazedonien (?)", "um 61–64", ""));
		bibel.add(new Bibelbuch(57, "Philemon", 1, Bibelbuecher.PHILEMON, "Paulus", "Rom", "um 60/61", ""));
		bibel.add(new Bibelbuch(58, "Hebräer", 13, Bibelbuecher.HEBRÄER, "Paulus", "Rom", "um 61", ""));
		bibel.add(new Bibelbuch(59, "Jakobus", 5, Bibelbuecher.JAKOBUS, "Jakobus (Bruder von Jesus)", "Jerusalem", "v. 62", ""));
		bibel.add(new Bibelbuch(60, "1. Petrus", 5, Bibelbuecher.ERSTER_PETRUS, "Petrus", "Babylon", "um 62–64", ""));
		bibel.add(new Bibelbuch(61, "2. Petrus", 3, Bibelbuecher.ZWEITER_PETRUS, "Petrus", "Babylon (?)", "um 64", ""));
		bibel.add(new Bibelbuch(62, "1. Johannes", 5, Bibelbuecher.ERSTER_JOHANNES, "Apostel Johannes", "Ephesus oder in der Nähe", "um 98", ""));
		bibel.add(new Bibelbuch(63, "2. Johannes", 1, Bibelbuecher.ZWEITER_JOHANNES, "Apostel Johannes", "Ephesus oder in der Nähe", "um 98", ""));
		bibel.add(new Bibelbuch(64, "3. Johannes", 1, Bibelbuecher.DRITTER_JOHANNES, "Apostel Johannes", "Ephesus oder in der Nähe", "um 98", ""));
		bibel.add(new Bibelbuch(65, "Judas", 1, Bibelbuecher.JUDAS, "Judas (Bruder von Jesus)", "Israel (?)", "um 65", ""));
		bibel.add(new Bibelbuch(66, "Offenbarung", 22, Bibelbuecher.OFFENBARUNG, "Apostel Johannes", "Patmos", "um 96", ""));
	}

	private Bibel() {
		// alles Util Daten Funktionen
	}

	/**
	 * Liefert alle Bibelschreiber.
	 * @param buch das Bibelbuch
	 * @return der Schreiber.
	 */
	public static String gebBibelschreiber(Bibelbuecher buch) {
		return bibel.get(buch.ordinal()).getSchreiber();
	}

	/**
	 * Liefert alle 66 Bibelbücher.
	 * 
	 * @return die Liste mit der ganzen Bibel
	 */
	public static List<Bibelbuch> getBibel() {
		return bibel;
	}

	/**
	 * Liefert die Summe aller Kapitel der ganzen Bibel (1189).
	 * 
	 * @return 1189 Bibelkapitel.
	 */
	public static int maxKapitel() {
		return bibel.parallelStream().map(buch -> buch.getMaxKapitel()).reduce(0, Integer::sum);
	}

	/**
	 * Liefert alle Kapitel (1189) der Bibel im Format: "Bibelbuchname Kapitel
	 * [Kapitel]".
	 * 
	 * @return alle möglichen Kapitel.
	 */
	public static List<String> getAlleKapitel() {

		List<String> alleKapitel = new ArrayList<>();

		Bibel.getBibel().stream().forEachOrdered(buch -> {

			// über jedes Kapitel
			for (int kapitel = 1; kapitel <= buch.getMaxKapitel(); kapitel++) {
				alleKapitel.add(String.format("%s" + TRENNER + "%d", buch.getName(), kapitel));
			}
		});
		return alleKapitel;
	}

	/**
	 * Liefert alle Kapitel des Bibelbuches der Bibel im Format: "Bibelbuchname
	 * Kapitel [Kapitel]".
	 * 
	 * @param bibelbuch das Bibelbuch was geliefert wird
	 * @return alle möglichen Kapitel des jeweiligen Bibelbuches
	 */
	public static List<String> getAlleKapitel(Bibelbuecher bibelbuch) {

		List<String> alleKapitel = new ArrayList<>();

		Bibel.getBibel().stream().forEachOrdered(buch -> {

			if (buch.getBibelbuch().equals(bibelbuch)) {
				// über jedes Kapitel
				for (int kapitel = 1; kapitel <= buch.getMaxKapitel(); kapitel++) {
					alleKapitel.add(String.format("%s" + TRENNER + "%d", buch.getName(), kapitel));
				}
			}
		});
		return alleKapitel;
	}

}
