package de.wenzlaff.twbibel;

/**
 * Ein Bibelbuch.
 * 
 * @author Thomas Wenzlaff
 */
public class Bibelbuch {

	/** Die Reihenfolge Nr. in der Bibel. */
	private Integer nrInBibel;
	/** Der Name des Bibelbuches. */
	private String name;
	/** Die maximale Anzahl von Kapitel diese Bibelbuches. */
	private Integer maxKapitel;
	/** Das Bibelbuch als Enum. */
	private Bibelbuecher bibelbuch;

	/** Der Schreiber des Bibelbuches. */
	private String schreiber;
	/** Wo das Bibelbuch geschrieben wurde. */
	private String woGeschrieben;
	/** Wann das Bibelbuch fertig geschrieben war. */
	private String wannFertig;
	/** Die Zeitspanne während das Bibelbuch geschrieben wurde. */
	private String zeitspanne;

	/**
	 * Konstruktor.
	 * 
	 * @param nrInBibel     die Nr. in der Bibel
	 * @param name          des Bibelbuches
	 * @param maxKapitel    das Kapitel
	 * @param bibelbuch     das Bibelbuch
	 * @param schreiber     der Schreiber
	 * @param woGeschrieben wo ist das Buch geschrieben
	 * @param wannFertig    wann war es Fertig
	 * @param zeitspanne    dauer
	 */
	public Bibelbuch(int nrInBibel, String name, int maxKapitel, Bibelbuecher bibelbuch, String schreiber, String woGeschrieben, String wannFertig, String zeitspanne) {

		this.nrInBibel = nrInBibel;
		this.name = name;
		this.maxKapitel = maxKapitel;
		this.bibelbuch = bibelbuch;
		this.schreiber = schreiber;
		this.woGeschrieben = woGeschrieben;
		this.wannFertig = wannFertig;
		this.zeitspanne = zeitspanne;
	}

	/**
	 * Liefert die NR. in der Bibel.
	 * 
	 * @return die Nr.
	 */
	public Integer getNrInBibel() {
		return nrInBibel;
	}

	/**
	 * Den Namen des Bibelbuches.
	 * 
	 * @return der Name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Die max. Kapitel.
	 * 
	 * @return die max. Kapitel
	 */
	public Integer getMaxKapitel() {
		return maxKapitel;
	}

	/**
	 * Der Bibelbuch Schreiber.
	 * 
	 * @return der Schreiber.
	 */
	public String getSchreiber() {
		return schreiber;
	}

	/**
	 * Wo ist das Buch geschrieben.
	 * 
	 * @return der Ort
	 */
	public String getWoGeschrieben() {
		return woGeschrieben;
	}

	/**
	 * Wann vollendet.
	 * 
	 * @return zeitpunkt der vollendung
	 */
	public String getWannFertig() {
		return wannFertig;
	}

	/**
	 * Dauer.
	 * 
	 * @return die Dauer der schreibung
	 */
	public String getZeitspanne() {
		return zeitspanne;
	}

	/**
	 * Ist das Bibelbuch im Alten Testament (heb. Schriften).
	 * 
	 * @return true wenn im AT, sonst false
	 */
	public boolean isAT() {
		return !isNT();
	}

	/**
	 * Ist das Bibelbuch im Neue Testament (gr. Schriften).
	 * 
	 * @return true wenn im NT, sonst false
	 */
	public boolean isNT() {
		return this.nrInBibel >= Bibelbuecher.MATTHÄUS.ordinal();
	}

	/**
	 * Liefert das Bibelbuch.
	 * 
	 * @return das Bibelbuch
	 */
	public Bibelbuecher getBibelbuch() {
		return bibelbuch;
	}

	/**
	 *
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nrInBibel == null) ? 0 : nrInBibel.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bibelbuch other = (Bibelbuch) obj;
		if (nrInBibel == null) {
			if (other.nrInBibel != null)
				return false;
		} else if (!nrInBibel.equals(other.nrInBibel))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Bibelbuch [");
		if (nrInBibel != null) {
			builder.append("nrInBibel=");
			builder.append(nrInBibel);
			builder.append(", ");
		}
		if (name != null) {
			builder.append("name=");
			builder.append(name);
			builder.append(", ");
		}
		if (maxKapitel != null) {
			builder.append("maxKapitel=");
			builder.append(maxKapitel);
		}
		builder.append("]");
		return builder.toString();
	}

}
