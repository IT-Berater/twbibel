package de.wenzlaff.twbibel;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.wenzlaff.bibelleseplan.Parameter;

/**
 * Zugriff für die Online Bibel von JW.org.
 * 
 * https://www.jw.org/de/bibliothek/bibel/
 * 
 * @author Thomas Wenzlaff
 */
public class JwOrg implements BibelUebersetzungen {

	private static final Logger LOG = LogManager.getLogger(JwOrg.class);

	private static final List<Uebersetzung> uebersetzungen = new ArrayList<>();

	static {
		// Reihenfolge "Abkürzung", "Name", "Sprache", "Bereich"));
		uebersetzungen.add(new Uebersetzung("RBI", "Neue-Welt-Übersetzung Studienbibel", "Deutsch", "AT/ NT",
				"https://www.jw.org/{sprache}/bibliothek/bibel/studienbibel/buecher/{Bibelbuch}/{Kapitel}/"));
		// https://www.jw.org/de/bibliothek/bibel/studienbibel/buecher/{Bibelbuch}/{Kapitel}/
		// https://www.jw.org/de/bibliothek/bibel/studienbibel/buecher/1-mose/26/

		uebersetzungen.add(new Uebersetzung("NWT", "Neue-Welt-Übersetzung Online Bibel (Revision 2018)", "Deutsch", "AT/ NT",
				"https://www.jw.org/{sprache}/bibliothek/bibel/nwt/bibelbuecher/{Bibelbuch}/{Kapitel}/"));
		// https://www.jw.org/de/bibliothek/bibel/nwt/bibelbuecher/{Bibelbuch}/{Kapitel}/
		// https://www.jw.org/de/bibliothek/bibel/nwt/bibelbuecher/3-mose/16/

		uebersetzungen.add(new Uebersetzung("BI12", "Neue-Welt-Übersetzung der Heiligen Schrift (1986)", "Deutsch", "AT/ NT",
				"https://www.jw.org/{sprache}/bibliothek/bibel/bi12/bibelbuecher/{Bibelbuch}/{Kapitel}/"));
		// https://www.jw.org/de/bibliothek/bibel/bi12/bibelbuecher/{Bibelbuch}/{Kapitel}/
		// https://www.jw.org/de/bibliothek/bibel/bi12/

		// TODO: Englisch, alle Bibelnamen sind im Link auf English!
		// uebersetzungen.add(new Uebersetzung("NWT", "New World Translation of the Holy
		// Scripture (Study Edition)", "English", "AT/ NT",
		// "https://www.jw.org/{sprache}/publikationen/bibel/nwt/bibelbuecher/{Bibelbuch}/{Kapitel}/"));
		// https://www.jw.org/en/library/bible/study-bible/books/{Bibelbuch}/{Kapitel}/
	}

	private static URL urlOnlineBibel;

	public JwOrg() {
		super();
	}

	/**
	 * Erzeugt die Online-Bibel.
	 * 
	 * @param parameter die Parameter
	 * @param kapitel   das Bibelkapitel
	 */
	public JwOrg(Parameter parameter, Integer kapitel) {

		try {
			urlOnlineBibel = getOnlineBibelLink(parameter, kapitel);
		} catch (MalformedURLException e) {
			LOG.error("Falsche URL zur Onlinebibel: " + urlOnlineBibel.getPath());
		}
	}

	/**
	 * Liefert den Link zur Online Bibel. Format für nwt:
	 * 
	 * //
	 * https://www.jw.org/de/publikationen/bibel/nwt/bibelbuecher/{Bibelbuch}/{Kapitel}/
	 * 
	 * @param parameter die Parameter
	 * @param kapitel   das Kapitel das angezeigt werden soll
	 * @return liefert den link zur Online Bibel
	 * @throws MalformedURLException wenn der Link zur Online-Bibel falsch ist
	 */
	public static URL getOnlineBibelLink(Parameter parameter, Integer kapitel) throws MalformedURLException {

		checkParameter(parameter, kapitel);

		String bibelBuchName = parameter.getBibelbuch().getName();
		String korrigierterBibelBuchName = fixNamen(bibelBuchName);
		Integer korrigiertesKapitel = fixKap(kapitel, parameter.getBibelbuch());

		LOG.debug("Name: " + korrigierterBibelBuchName + " Kapitel: " + korrigiertesKapitel);

		URL onlineUrl = URI.create(String.format(getBasisUrl(parameter), parameter.getSprache(), korrigierterBibelBuchName, korrigiertesKapitel)).toURL();

		return onlineUrl;
	}

	/**
	 * Liefert die Basis Url für die jeweilige Übersetzung.
	 * 
	 * @param parameter die Parameter
	 * @return die Basis URL oder ""
	 */
	public static String getBasisUrl(Parameter parameter) {
		String url = "";

		String bibelübersetzung = parameter.getBibelübersetzung();

		if (bibelübersetzung.startsWith("NWT")) {
			// https://www.jw.org/de/bibliothek/bibel/nwt/bibelbuecher/3-mose/16/
			// https://www.jw.org/de/bibliothek/bibel/nwt/bibelbuecher/{Bibelbuch}/{Kapitel}/
			url = "https://www.jw.org/%s/bibliothek/bibel/nwt/bibelbuecher/%s/%s/"; // Reihenfolge: sprache, bibelbuchname, kapitel.

		} else if (bibelübersetzung.startsWith("RBI")) {
			// https://www.jw.org/de/bibliothek/bibel/studienbibel/buecher/{Bibelbuch}/{Kapitel}/
			url = "https://www.jw.org/%s/bibliothek/bibel/studienbibel/buecher/%s/%s/";

		} else if (bibelübersetzung.startsWith("BI12")) {
			// https://www.jw.org/de/bibliothek/bibel/bi12/buecher/{Bibelbuch}/{Kapitel}/
			url = "https://www.jw.org/%s/bibliothek/bibel/bi12/buecher/%s/%s/";

		} else {
			throw new KeineLinkErzeugungException("Unbekannte Bibelübersetzung: " + parameter);
		}
		return url;
	}

	private static void checkParameter(Parameter parameter, Integer kapitel) {
		if (parameter.getSprache() == null) {
			throw new KeineLinkErzeugungException("Keine Sprache gesetzt, keine Link erzeugung möglich");
		}
		if (parameter.getBibelübersetzung() == null) {
			throw new KeineLinkErzeugungException("Keine Bibelübersetzung angegeben, keine Link erzeugung möglich");
		}
		if (parameter.getBibelbuch() == null) {
			throw new KeineLinkErzeugungException("Keine Bibelbuch angegeben, keine Link erzeugung möglich");
		}
		if (kapitel == null) {
			throw new KeineLinkErzeugungException("Kein Kapitel angegeben, keine Link erzeugung möglich");
		}
	}

	/**
	 * Checkt das Kapitel. Wenn es größer als vorhanden ist, wird das letzt Kapitel
	 * geliefert. Wenn es kleiner 1 ist wird Kapitel 1 gelifert.
	 * 
	 * @param kap
	 * @param buch
	 * @return das Kapitel
	 */
	private static Integer fixKap(Integer kap, Bibelbuecher buch) {
		Integer kapitel = kap;
		if (kap > buch.getMaxKapitel()) {
			kapitel = buch.getMaxKapitel();
		}
		if (kap < 1) {
			kapitel = 1;
		}
		return kapitel;
	}

	private static String fixNamen(String namen) {
		if (namen.startsWith("Hohes")) {
			namen = namen.replace("Hohes Lied", "hohes-lied");
		} else if (namen.startsWith("1.")) {
			namen = namen.replace("1. ", "1-").toLowerCase();
		} else if (namen.startsWith("2.")) {
			namen = namen.replace("2. ", "2-").toLowerCase();
		} else if (namen.startsWith("3.")) {
			namen = namen.replace("3. ", "3-").toLowerCase();
		} else if (namen.startsWith("4.")) {
			namen = namen.replace("4. ", "4-").toLowerCase();
		} else if (namen.startsWith("5.")) {
			namen = namen.replace("5. ", "5-").toLowerCase();
		}
		return namen;
	}

	@Override
	public URL getOnlineBibelURL() {
		LOG.debug("Erzeuge URL zur Online-Bibel: " + urlOnlineBibel);
		return urlOnlineBibel;
	}

	@Override
	public List<Uebersetzung> getUebersetzungen() {
		return uebersetzungen;
	}
}