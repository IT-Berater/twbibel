package de.wenzlaff.twbibel.gui;

import java.awt.Desktop;
import java.io.File;
import java.net.URI;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.wenzlaff.bibelleseplan.Kommandozeile;
import de.wenzlaff.bibelleseplan.Parameter;
import de.wenzlaff.twbibel.BibelUebersetzungen;
import de.wenzlaff.twbibel.Bibelbuecher;
import de.wenzlaff.twbibel.BibelserverCom;
import de.wenzlaff.twbibel.JwOrg;
import de.wenzlaff.twbibel.Uebersetzung;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.stage.FileChooser;

/**
 * Kontroller füe die GUI.
 * 
 * @author Thomas Wenzlaff
 */
public class Controller {

	private static final Logger LOG = LogManager.getLogger(Controller.class);

	private static final String DEFAULT_BIBELSPRACHE_DEUTSCH = "Deutsch";

	/** Die Position (Index) der Vorbelegten Bibelübersetzung. */
	private static final int DEFAULT_ÜBERSETZUNG_NWT = 9;

	@FXML
	private Button genierierenButton;

	@FXML
	private Button abbruchButton;

	@FXML
	private ChoiceBox<Bibelbuecher> bibelbuch;

	@FXML
	private ChoiceBox<Integer> kapitelprotag;

	@FXML
	private Label ergebnis;

	@FXML
	private DatePicker startdatum;

	@FXML
	private Hyperlink homepage;

	@FXML
	private ChoiceBox<Uebersetzung> bibelübersetzung;

	@FXML
	private ChoiceBox<String> sprache;

	/** Alle möglichen Bibelübersetzung. */
	private List<Uebersetzung> alleBibleübersetzungen;

	/**
	 * Init.
	 */
	@FXML
	public void initialize() {

		// Anzahl Kapitel pro Tag
		ObservableList<Integer> count = FXCollections.observableArrayList();

		for (int i = 1; i < 11; i++) {
			count.add(Integer.valueOf(i));
		}

		kapitelprotag.setItems(count);
		kapitelprotag.getSelectionModel().select(2);
		Tooltip t1 = new Tooltip("Auswahl wieviel Kapitel/Termine an einem Tag gelesen werden sollen.");
		kapitelprotag.setTooltip(t1);

		// Bibelbücher
		ObservableList<Bibelbuecher> alleBibelbuecher = FXCollections.observableArrayList(Bibelbuecher.values());

		bibelbuch.setItems(alleBibelbuecher);
		Tooltip t2 = new Tooltip("Wähle hier das Bibelbuch. Wenn es leer ist, wird die ganze Bibel generiert.");
		bibelbuch.setTooltip(t2);

		// Startdatum
		startdatum.setValue(LocalDate.now());
		Tooltip t3 = new Tooltip("Setzt das Startdatum ab wann das Leseprogramm starten soll.");
		startdatum.setTooltip(t3);

		// Ergebnis
		ergebnis.setText("");

		iniAlleBibelübersetzungen();

		initChoiceBoxBibelübersetzungen(bibelübersetzung, alleBibleübersetzungen, "Die verwendete Bibleübersetzung.");

		// Sprachen
		List<String> alleSprachen = new ArrayList<>();

		alleBibleübersetzungen.stream().sorted().forEach(u -> {
			if (!alleSprachen.contains(u.getSprache()) && u.getSprache().isEmpty() == false) {
				alleSprachen.add(u.getSprache());
			}
		});

		initChoiceBoxSprachen(sprache, alleSprachen, "Die Sprache der Bibleübersetzung.");

		// wenn sich die Sprache ändert, die Bibelübersetzungen ändern...
		sprache.getSelectionModel().selectedItemProperty()
				.addListener((ObservableValue<? extends String> observable, String alteSprache, String neueSprache) -> spracheVerändern(neueSprache));

		// ini
		sprache.getSelectionModel().select(DEFAULT_BIBELSPRACHE_DEUTSCH);
		bibelübersetzung.getSelectionModel().select(DEFAULT_ÜBERSETZUNG_NWT);
	}

	private void iniAlleBibelübersetzungen() {

		alleBibleübersetzungen = new ArrayList<>();

		BibelUebersetzungen nwü = new JwOrg();
		List<Uebersetzung> nwüÜbersetzungen = nwü.getUebersetzungen();
		alleBibleübersetzungen.addAll(nwüÜbersetzungen);

		BibelUebersetzungen bibelServerCom = new BibelserverCom();
		List<Uebersetzung> bibelServerComUebersetzungen = bibelServerCom.getUebersetzungen();
		alleBibleübersetzungen.addAll(bibelServerComUebersetzungen);

		Collections.sort(alleBibleübersetzungen);
	}

	private void initChoiceBoxSprachen(ChoiceBox<String> choiceBox, List<String> view, String tooltip) {

		ObservableList<String> obserView = FXCollections.observableArrayList(view);

		choiceBox.setItems(obserView);
		choiceBox.getSelectionModel().select(0);
		Tooltip toolTip = new Tooltip(tooltip);
		choiceBox.setTooltip(toolTip);
	}

	private void initChoiceBoxBibelübersetzungen(ChoiceBox<Uebersetzung> choiceBox, List<Uebersetzung> view, String tooltip) {

		ObservableList<Uebersetzung> obserView = FXCollections.observableArrayList(view);

		choiceBox.setItems(obserView);

		Tooltip toolTip = new Tooltip(tooltip);
		choiceBox.setTooltip(toolTip);
	}

	private Object spracheVerändern(String neueSprache) {
		LOG.info("Sprache wurde geändert auf: " + neueSprache);

		List<Uebersetzung> gefiltertNachSprachen = new ArrayList<>();
		alleBibleübersetzungen.stream().sorted().forEach(s -> {
			if (s.getSprache().contains(neueSprache) && s.getSprache().isEmpty() == false) {
				gefiltertNachSprachen.add(s);
			}
		});

		ObservableList<Uebersetzung> übersetzungenNachSprache = FXCollections.observableArrayList(gefiltertNachSprachen);

		bibelübersetzung.setItems(übersetzungenNachSprache);

		if (neueSprache.equals(DEFAULT_BIBELSPRACHE_DEUTSCH)) {
			bibelübersetzung.getSelectionModel().select(DEFAULT_ÜBERSETZUNG_NWT);
		} else {
			bibelübersetzung.getSelectionModel().select(0);
		}

		return null;
	}

	/**
	 * Genieriert den Kalender.
	 * 
	 * @param event
	 */
	@FXML
	void generiereKalender(ActionEvent event) {

		File ausgewaehlterAusgabeDateiname = zeigeDateiSpeichernDialog();

		if (ausgewaehlterAusgabeDateiname != null) {

			LOG.info("Speicher nach: " + ausgewaehlterAusgabeDateiname.getAbsolutePath());
			ergebnis.setText("Bibelleseplan generieren ...");

			String[] args;

			String startDatumParameter;

			if (startdatum.getValue() == null) {
				startDatumParameter = LocalDate.now().toString();
			} else {
				startDatumParameter = startdatum.getValue().toString();
			}

			// Todo andere Sprachen
			String gewSprache = sprache.getSelectionModel().getSelectedItem().toString();
			if (gewSprache.equals(DEFAULT_BIBELSPRACHE_DEUTSCH)) {
				gewSprache = "de";
			}

			if (isBibelbuchAusgewählt()) {

				args = new String[] { "-s", startDatumParameter, "-b", bibelbuch.getSelectionModel().getSelectedItem().toString(), "-k",
						kapitelprotag.getSelectionModel().getSelectedItem().toString(), "-a", ausgewaehlterAusgabeDateiname.getAbsolutePath(), "-l", gewSprache, "-u",
						bibelübersetzung.getSelectionModel().getSelectedItem().toString() };

			} else {
				// ganze Bibel gewählt
				args = new String[] { "-s", startDatumParameter, "-k", kapitelprotag.getSelectionModel().getSelectedItem().toString(), "-a",
						ausgewaehlterAusgabeDateiname.getAbsolutePath(), "-l", gewSprache, "-u", bibelübersetzung.getSelectionModel().getSelectedItem().toString() };
			}

			try {

				Kommandozeile.main(args);

			} catch (Exception e) {
				LOG.error("Fehler beim generieren: " + e.getCause(), e);
			}

			ergebnis.setText("Bibelleseplan gespeicher: " + ausgewaehlterAusgabeDateiname.getAbsolutePath());

			bibelbuch.getSelectionModel().clearSelection();

			startdatum.setValue(LocalDate.now());
		}
	}

	private boolean isBibelbuchAusgewählt() {
		return bibelbuch.getSelectionModel().getSelectedItem() != null;
	}

	private File zeigeDateiSpeichernDialog() {
		FileChooser fileChooser = new FileChooser();

		FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("ICS Kalender Dateien (*.ics)", "*.ics");
		fileChooser.getExtensionFilters().add(extFilter);
		fileChooser.setTitle("Speicher erzeugten Bibelleseplan");

		ausgabeNameVorbelegen(fileChooser);

		File ausgewaehlterAusgabeDateiname = fileChooser.showSaveDialog(null);
		return ausgewaehlterAusgabeDateiname;
	}

	private String ausgabeNameVorbelegen(FileChooser fileChooser) {
		Parameter benutzerAuswahl = new Parameter();
		if (isBibelbuchAusgewählt()) {
			benutzerAuswahl.setBibelbuch(bibelbuch.getSelectionModel().getSelectedItem().toString());
		} else {
			benutzerAuswahl.setBibelbuch((String) null);
		}

		if (startdatum.getValue() == null) {
			benutzerAuswahl.setStartDatum(LocalDate.now().toString());
		} else {
			benutzerAuswahl.setStartDatum(startdatum.getValue().toString());
		}

		benutzerAuswahl.setBibelübersetzung(bibelübersetzung.getSelectionModel().getSelectedItem().toString());

		String dateiname = DateiNamen.getIcsDateinamen(benutzerAuswahl);
		fileChooser.setInitialFileName(dateiname);

		return dateiname;
	}

	@FXML
	void abbruch(ActionEvent event) {
		Platform.exit();
		System.exit(0);
	}

	@FXML
	void homepage(ActionEvent event) throws Exception {
		if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
			Desktop.getDesktop().browse(new URI("http://www.wenzlaff.info"));
		}
	}
}