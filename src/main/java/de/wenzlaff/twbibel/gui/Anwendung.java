package de.wenzlaff.twbibel.gui;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * Start Klasse der Gui.
 * 
 * Starten der Anwendung via: clean javafx:run
 * 
 * 
 * JDK 19 https://jdk.java.net/19/ JavaFX https://gluonhq.com/products/javafx/
 * 
 * Anleitung: https://openjfx.io/openjfx-docs/
 * 
 * Eclipse:
 * 
 * https://openjfx.io/openjfx-docs/#maven
 * 
 * @author Thomas Wenzlaff
 *
 */
public class Anwendung extends Application {

	private static final Logger LOG = LogManager.getLogger(Anwendung.class);

	private static final String APP_NAME = "TWBibel";

	/**
	 * Start.
	 * 
	 * @param stage Stage
	 */
	@Override
	public void start(Stage stage) {
		try {
			stage.setTitle(APP_NAME);
			stage.getIcons().add(new Image(Anwendung.class.getResourceAsStream("buch.png")));
			stage.setScene(new Scene(loadFXML("MainScene")));
			stage.show();

		} catch (Exception e) {
			LOG.error("Fehler: " + e.getLocalizedMessage(), e);
		}
	}

	/**
	 * Laden des fxml.
	 * 
	 * @param fxml das fxml.
	 * @return den Parent
	 * @throws IOException
	 */
	private Parent loadFXML(String fxml) throws IOException {
		FXMLLoader fxmlLoader = new FXMLLoader(Anwendung.class.getResource(fxml + ".fxml"));
		return fxmlLoader.load();
	}

	/**
	 * Aufruf.
	 * 
	 * @param args Programmargumente.
	 */
	public static void main(String[] args) {
		LOG.info("Start " + APP_NAME);
		launch(args);
	}
}