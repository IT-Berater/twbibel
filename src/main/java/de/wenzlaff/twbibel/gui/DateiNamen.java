package de.wenzlaff.twbibel.gui;

import de.wenzlaff.bibelleseplan.Parameter;

/**
 * Klasse für die Erzeugung von Dateinamen.
 * 
 * @author Thomas Wenzlaff
 *
 */
public class DateiNamen {

	/**
	 * Liefert den Dateinamen mit einigen Parametern.
	 * 
	 * @param parameter die Parameter
	 * @return der Dateiname z.B. bibelleseplan_ganze_bibel_NWT_ab_23_8_2020.ics
	 */
	public static String getIcsDateinamen(Parameter parameter) {

		String bibelbuchName;

		if (parameter.getBibelbuch() == null) {
			bibelbuchName = "ganze_bibel";
		} else {
			bibelbuchName = parameter.getBibelbuch().name();

		}

		String abkDerBibelübersetzung = "";
		if (parameter.getBibelübersetzung() != null) {
			// BI12 - Neue-Welt-Übersetzung der Heiligen Schrift (1986)
			abkDerBibelübersetzung = parameter.getBibelübersetzung().split("-")[0];
		}

		return "bibelleseplan_" + bibelbuchName + "_" + abkDerBibelübersetzung + "_ab_" + getStartDatum(parameter) + ".ics";
	}

	private static String getStartDatum(Parameter parameter) {
		return parameter.getStartDatum().getDayOfMonth() + "_" + parameter.getStartDatum().getMonthValue() + "_" + parameter.getStartDatum().getYear();
	}

}
