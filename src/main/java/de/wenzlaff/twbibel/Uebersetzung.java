package de.wenzlaff.twbibel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Eine Bibelübersetzung.
 * 
 * @author Thomas Wenzlaff
 *
 */
public class Uebersetzung implements Comparable<Uebersetzung> {

	private static final Logger LOG = LogManager.getLogger(Uebersetzung.class.getName());

	private static final String TRENNZEICHEN = " - ";

	/**
	 * Die Abkürzung der Übersetzung, z.B. LUT für Luther.
	 */
	private String abkuerzung;
	/**
	 * Der Name der Übersetzung ausgeschieben, z.B. Lutherbibel 2017
	 */
	private String name;
	/**
	 * Die Sprache der Bibelübersetzung.
	 */
	private String sprache;
	/**
	 * Der Bereich in dem die Übersetzung vorliegt. Z.B. AT/ NT/ Apokryphen mit
	 * Trenner /
	 */
	private String bereich;

	/** Der Link wo die Bibelübersetzung im Internet zu erreichen ist. */
	private String onlineLink;

	/**
	 * Eine Übersetzung anlegen.
	 * 
	 * @param abkuerzung z.b. LUT
	 * @param name       z.B. Lutherbibel 2017
	 * @param sprache    z.B. Deutsch
	 * @param bereich    z.B. AT/ NT/ Apokryphen
	 * @param onlineLink der Link wo die Bibel zu finden ist, mit Platzhalter für
	 *                   Sprache
	 */
	public Uebersetzung(String abkuerzung, String name, String sprache, String bereich, String onlineLink) {
		LOG.debug("Konstruktor: " + name);
		this.abkuerzung = abkuerzung;
		this.name = name;
		this.sprache = sprache;
		this.bereich = bereich;
		this.onlineLink = onlineLink;
	}

	/**
	 * Liefert die Abkürzung der Bibelübersetzung z.B. LUT
	 * 
	 * @return die Abk. der Bibelübersetzung.
	 */
	public String getAbkuerzung() {
		return abkuerzung;
	}

	public String getName() {
		return name;
	}

	public String getSprache() {
		return sprache;
	}

	public String getBereich() {
		return bereich;
	}

	public String getOnlineLink() {
		return onlineLink;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((abkuerzung == null) ? 0 : abkuerzung.hashCode());
		result = prime * result + ((bereich == null) ? 0 : bereich.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((sprache == null) ? 0 : sprache.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Uebersetzung other = (Uebersetzung) obj;
		if (abkuerzung == null) {
			if (other.abkuerzung != null)
				return false;
		} else if (!abkuerzung.equals(other.abkuerzung))
			return false;
		if (bereich == null) {
			if (other.bereich != null)
				return false;
		} else if (!bereich.equals(other.bereich))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (sprache == null) {
			if (other.sprache != null)
				return false;
		} else if (!sprache.equals(other.sprache))
			return false;
		return true;
	}

	/**
	 * Für die Ansicht in der ComboBox.
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(getAbkuerzung());
		builder.append(TRENNZEICHEN);
		builder.append(getName());
		return builder.toString();
	}

	@Override
	public int compareTo(Uebersetzung o) {

		return this.getAbkuerzung().compareTo(o.getAbkuerzung());
	}

}
