package de.wenzlaff.twbibel;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Siehe https://www.bibleserver.com/webmasters/
 * 
 * Alle möglichkeiten die der Bibelserver.com bietet
 * 
 * @author Thomas Wenzlaff
 *
 */
public class BibelserverCom implements BibelUebersetzungen {

	private static final Logger LOG = LogManager.getLogger(BibelserverCom.class.getName());

	private static final List<Uebersetzung> uebersetzungen = new ArrayList<>();

	static {
		// Reihenfolge "Abkürzung", "Name", "Sprache", "Bereich"));
		uebersetzungen.add(new Uebersetzung("LUT", "Lutherbibel 2017", "Deutsch", "AT/ NT/ Apokryphen", null));
		uebersetzungen.add(new Uebersetzung("ELB", "Elberfelder Bibel", "Deutsch", "AT/ NT", null));
		uebersetzungen.add(new Uebersetzung("HFA", "Hoffnung für Alle", "Deutsch", "AT/ NT", null));
		uebersetzungen.add(new Uebersetzung("SLT", "Schlachter 2000", "Deutsch", "AT/ NT", null));
		uebersetzungen.add(new Uebersetzung("ZB", "Zürcher Bibel", "Deutsch", "AT/ NT", null));
		uebersetzungen.add(new Uebersetzung("NGÜ", "Neue Genfer Übersetzung", "Deutsch", "Psalmen/ NT", null));
		uebersetzungen.add(new Uebersetzung("GNB", "Gute Nachricht Bibel", "Deutsch", "AT/ NT/ Apokryphen", null));
		uebersetzungen.add(new Uebersetzung("EU", "Einheitsübersetzung 2016", "Deutsch", "AT/ NT/ Apokryphen", null));
		uebersetzungen.add(new Uebersetzung("ESV", "English Standard Version", "Englisch", "AT/ NT", null));
		uebersetzungen.add(new Uebersetzung("NLB", "Neues Leben. Die Bibel", "Deutsch", "AT/ NT", null));
		uebersetzungen.add(new Uebersetzung("NIV", "New International Version", "Englisch", "AT/ NT", null));
		uebersetzungen.add(new Uebersetzung("NeÜ", "Neue evangelistische Übersetzung", "Deutsch", "AT/ NT", null));
		uebersetzungen.add(new Uebersetzung("MENG", "Menge Bibel", "Deutsch", "AT/ NT/ Apokryphen", null));
		uebersetzungen.add(new Uebersetzung("NIRV", "New Int. Readers Version", "Englisch", "AT/ NT", null));
		uebersetzungen.add(new Uebersetzung("KJV", "King James Version", "Englisch", "AT/ NT", null));
		uebersetzungen.add(new Uebersetzung("BDS", "Bible du Semeur", "Französisch", "AT/ NT", null));
		uebersetzungen.add(new Uebersetzung("S21", "Segond 21", "Französisch", "AT/ NT", null));
		uebersetzungen.add(new Uebersetzung("ITA", "La Parola è Vita", "Italienisch", "NT", null));
		uebersetzungen.add(new Uebersetzung("NRS", "Nuova Riveduta 2006", "Italienisch", "AT/ NT", null));
		uebersetzungen.add(new Uebersetzung("HTB", "Het Boek", "Holländisch", "AT/ NT", null));
		uebersetzungen.add(new Uebersetzung("LSG", "Louis Segond 1910", "Französisch", "AT/ NT", null));
		uebersetzungen.add(new Uebersetzung("CST", "Nueva Versión Internacional (Castilian)", "Spanisch", "AT/ NT", null));
		uebersetzungen.add(new Uebersetzung("NVI", "Nueva Versión Internacional", "Spanisch", "AT/ NT", null));
		uebersetzungen.add(new Uebersetzung("BTX", "La Biblia Textual", "Spanisch", "AT/ NT", null));
		uebersetzungen.add(new Uebersetzung("PRT", "O Livro", "Portugiesisch", "AT/ NT", null));
		uebersetzungen.add(new Uebersetzung("NOR", "En Levende Bok", "Norwegisch", "NT", null));
		uebersetzungen.add(new Uebersetzung("BSV", "Nya Levande Bibeln", "Schwedisch", "AT/ NT", null));
		uebersetzungen.add(new Uebersetzung("DK", "Bibelen på hverdagsdansk", "Dänisch", "AT/ NT", null));
		uebersetzungen.add(new Uebersetzung("PSZ", "Słowo Życia", "Polnisch", "NT", null));
		uebersetzungen.add(new Uebersetzung("CEP", "Český ekumenický překlad", "Tschechisch", "AT/ NT/ Apokryphen", null));
		uebersetzungen.add(new Uebersetzung("SNC", "Slovo na cestu", "Tschechisch", "AT/ NT", null));
		uebersetzungen.add(new Uebersetzung("B21", "Bible/ překlad 21. století", "Tschechisch", "AT/ NT/ Apokryphen", null));
		uebersetzungen.add(new Uebersetzung("BKR", "Bible Kralická", "Tschechisch", "AT/ NT", null));
		uebersetzungen.add(new Uebersetzung("NPK", "Nádej pre kazdého", "Slowakisch", "NT", null));
		uebersetzungen.add(new Uebersetzung("KAR", "IBS-fordítás (Új Károli)", "Ungarisch", "AT", null));
		uebersetzungen.add(new Uebersetzung("HUN", "Hungarian", "Ungarisch", "NT", null));
		uebersetzungen.add(new Uebersetzung("NTR", "Noua traducere în limba românã", "Rumänisch", "AT/ NT", null));
		uebersetzungen.add(new Uebersetzung("BGV", "Верен", "Bulgarisch", "AT/ NT", null));
		uebersetzungen.add(new Uebersetzung("CBT", "Библия/ нов превод от оригиналните езици", "Bulgarisch", "AT/ NT/ Apokryphen", null));
		uebersetzungen.add(new Uebersetzung("CKK", "Knjiga O Kristu", "Kroatisch", "NT", null));
		uebersetzungen.add(new Uebersetzung("RSZ", "Новый перевод на русский язык", "Russisch", "AT/ NT", null));
		uebersetzungen.add(new Uebersetzung("CARS", "Священное Писание/ Восточный перевод", "Russisch", "AT/ NT", null));
		uebersetzungen.add(new Uebersetzung("TR", "Türkçe", "Türkisch", "AT/ NT", null));
		uebersetzungen.add(new Uebersetzung("NAV", "Ketab El Hayat", "Arabisch", "AT/ NT", null));
		uebersetzungen.add(new Uebersetzung("FCB", "کتاب مقدس، ترجمه تفسیری", "Farsi", "AT/ NT", null));
		uebersetzungen.add(new Uebersetzung("CUVS", "中文和合本（简体）", "Chinesisch", "AT/ NT", null));
		uebersetzungen.add(new Uebersetzung("CCBT", "聖經當代譯本修訂版", "Chinesisch", "AT/ NT", null));
		uebersetzungen.add(new Uebersetzung("OT", "Hebrew OT", "", "AT", null));
		uebersetzungen.add(new Uebersetzung("LXX", "Septuaginta", "", "AT", null));
	}

	@Override
	public List<Uebersetzung> getUebersetzungen() {
		return uebersetzungen;
	}

	@Override
	public URL getOnlineBibelURL() {
		LOG.debug("Erzeuge URL zur Online-Bibel: ");
		// TODO ...
		return null;
	}

}
