package de.wenzlaff.twbibel;

/**
 * Alle 66 Bibelbücher.
 * 
 * In der richtigen Reihenfolge.
 * 
 * 1.,2. und 3. durch ERSTER_, ZWEITER_ und DRITER_ ersetzt.
 * 
 * @author Thomas Wenzlaff
 */
public enum Bibelbuecher {

	// AT
	/** 1. Mose (Genesis). */
	ERSTER_MOSE,
	/** 2. Mose. */
	ZWEITER_MOSE, DRITTER_MOSE, VIERTER_MOSE, FUENFTER_MOSE, JOSUA, RICHTER, RUTH, ERSTER_SAMUEL, ZWEITER_SAMUEL, ERSTER_KÖNIGE, ZWEITER_KÖNIGE, ERSTER_CHRONIKA, ZWEITER_CHRONIKA,
	ESRA, NEHEMIA, ESTHER, HIOB, PSALMEN, SPRÜCHE, PREDIGER, HOHESLIED, JESAJA, JEREMIA, KLAGELIEDER, HESEKIEL, DANIEL, HOSEA, JOEL, AMOS, OBADJA, JONA, MICHA, NAHUM, HABAKUK,
	ZEPHANJA, HAGGAI, SACHARJA, MALEACHI,
	// NT
	MATTHÄUS, MARKUS, LUKAS, JOHANNES, APOSTELGESCHICHTE, RÖMER, ERSTER_KORINTHER, ZWEITER_KORINTHER, GALATER, EPHESER, PHILIPPER, KOLOSSER, ERSTER_THESSALONICHER,
	ZWEITER_THESSALONICHER, ERSTER_TIMOTHEUS, ZWEITER_TIMOTHEUS, TITUS, PHILEMON, HEBRÄER, JAKOBUS, ERSTER_PETRUS, ZWEITER_PETRUS, ERSTER_JOHANNES, ZWEITER_JOHANNES,
	DRITTER_JOHANNES, JUDAS, OFFENBARUNG;

	/**
	 * Liefert alle Bibelbücher Namen als Enum mit Komma getrent.
	 * 
	 * @return String mit allen Kommaseparierten Bibelbüchern.
	 */
	public static String getAlleBibelbuecher() {
		StringBuffer b = new StringBuffer();
		for (Bibelbuecher buch : Bibelbuecher.values()) {
			b.append(buch);
			b.append(", ");
		}

		return b.toString();
	}

	/**
	 * Liefert den Bibelschreiber.
	 * 
	 * @return der Bibelschreiber
	 */
	public String getBibelschreiber() {
		return Bibel.getBibel().get(this.ordinal()).getSchreiber();
	}

	/**
	 * Die max. Kapitel.
	 * 
	 * @return max. Kapitel
	 */
	public Integer getMaxKapitel() {
		return Bibel.getBibel().get(this.ordinal()).getMaxKapitel();
	}

	/**
	 * Die Nr. in der Bibel
	 * 
	 * @return nr.
	 */
	public Integer getNrInBibel() {
		return Bibel.getBibel().get(this.ordinal()).getNrInBibel();
	}

	/**
	 * Den Namen des Bibelbuches
	 * 
	 * @return bibelbuch Namen
	 */
	public String getName() {
		return Bibel.getBibel().get(this.ordinal()).getName();

	}

	@Override
	public String toString() {
		return fixNamen(getName());
	}

	private static String fixNamen(String namen) {
		if (namen.startsWith("HohesLied")) {
			namen = namen.replace("HohesLied", "Hohes Lied");
		} else if (namen.startsWith("ERSTER")) {
			namen = namen.replace("ERSTER_", "1. ");
		} else if (namen.startsWith("ZWEITER")) {
			namen = namen.replace("ZWEITER_", "2. ");
		} else if (namen.startsWith("DRITTER")) {
			namen = namen.replace("DRITTER_", "3. ");
		} else if (namen.startsWith("VIERTER")) {
			namen = namen.replace("VIERTER_", "4. ");
		} else if (namen.startsWith("FUENFTER")) {
			namen = namen.replace("FUENFTER_", "5. ");
		}
		return namen;
	}

	/**
	 * Wandelt alle echten Bibelbuchnamen wie 1. Mose in das Bibelbuecher Enum um.
	 * (Hilfsschreibweise der Enums um z.B. ERSTER_MOSE).
	 * 
	 * @param realBibelbuchNamen der Bibelbuch Name
	 * @return das Enum Bibelbucher
	 */
	public static Bibelbuecher fixNamenToBibelbuecher(String realBibelbuchNamen) {
		if (realBibelbuchNamen.startsWith("Hohes")) {
			realBibelbuchNamen = realBibelbuchNamen.replace("Hohes Lied", "HohesLied");
		} else if (realBibelbuchNamen.startsWith("1.")) {
			realBibelbuchNamen = realBibelbuchNamen.replace("1. ", "ERSTER_");
		} else if (realBibelbuchNamen.startsWith("2.")) {
			realBibelbuchNamen = realBibelbuchNamen.replace("2. ", "ZWEITER_");
		} else if (realBibelbuchNamen.startsWith("3.")) {
			realBibelbuchNamen = realBibelbuchNamen.replace("3. ", "DRITTER_");
		} else if (realBibelbuchNamen.startsWith("4.")) {
			realBibelbuchNamen = realBibelbuchNamen.replace("4. ", "VIERTER_");
		} else if (realBibelbuchNamen.startsWith("5.")) {
			realBibelbuchNamen = realBibelbuchNamen.replace("5. ", "FUENFTER_");
		}
		return Bibelbuecher.valueOf(realBibelbuchNamen.toUpperCase());
	}
}
