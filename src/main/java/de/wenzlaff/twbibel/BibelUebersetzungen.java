package de.wenzlaff.twbibel;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import de.wenzlaff.bibelleseplan.Parameter;

/**
 * Das Interface für alle Bibel-Übersetzungen.
 * 
 * @author Thomas Wenzlaff
 *
 */
public interface BibelUebersetzungen {
	/**
	 * Liefert alle möglichen Übersetzungen.
	 * 
	 * @return liste mit Übersetzungen.
	 */
	List<Uebersetzung> getUebersetzungen();

	/**
	 * Liefert den Link zur Online Bibel
	 * 
	 * @return Link zur Online Bible
	 */
	URL getOnlineBibelURL();

	/**
	 * Liefert alle Abkürzungen mit Komma getrennt als Hilfe String.
	 * 
	 * @param uebersetzung die Übersetzungen
	 * @return alle möglichen Übersetzungen
	 */
	@SafeVarargs
	static String getAllAbk(List<Uebersetzung>... uebersetzung) {

		StringBuffer erg = new StringBuffer();

		for (List<Uebersetzung> ue : uebersetzung) {

			ue.stream().sorted().forEach(u -> {
				erg.append(u.getAbkuerzung());
				erg.append(", ");
			});
		}

		return erg.toString().substring(0, erg.length() - 2);
	}

	/**
	 * Liefert alle Abkürzungen mit Komma getrennt als Hilfe String sortiert.
	 * 
	 * @return alle sortiert zusammen
	 */
	static String getAllAbkSortiert() {

		StringBuffer erg = new StringBuffer();
		BibelserverCom bibelserver = new BibelserverCom();

		List<Uebersetzung> alleBibelserver = bibelserver.getUebersetzungen();

		Parameter parameter = new Parameter();
		parameter.setSprache("de");
		parameter.setBibelübersetzung("NWT");
		parameter.setBibelbuch(Bibelbuecher.ERSTER_MOSE.toString());

		JwOrg jw = new JwOrg(parameter, 1);
		List<Uebersetzung> alleJwOrg = jw.getUebersetzungen();

		List<Uebersetzung> alleUebersetzungen = new ArrayList<>();
		alleUebersetzungen.addAll(alleBibelserver);
		alleUebersetzungen.addAll(alleJwOrg);

		alleUebersetzungen.stream().sorted().forEach(u -> {
			erg.append(u.getAbkuerzung());
			erg.append(", ");
		});

		return erg.toString().substring(0, erg.length() - 2);
	}
}