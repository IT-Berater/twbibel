package de.wenzlaff.twbibel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Exception für den Fall das keine Erzeugung des Online Links möglich ist.
 * 
 * @author Thomas Wenzlaff
 *
 */
public class KeineLinkErzeugungException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private static final Logger LOG = LogManager.getLogger(JwOrg.class);

	public KeineLinkErzeugungException(String fehlermeldung) {
		super(fehlermeldung);
		LOG.error(fehlermeldung);
	}
}
