package de.wenzlaff.bibelleseplan;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Datei Archiv erstellen.
 * 
 * @author Thomas Wenzlaff
 */
public class Archiv {

	private static final Logger LOG = LogManager.getLogger(Archiv.class);

	private static final String DATEI_EXTENSION_ZIP = ".zip";

	/**
	 * Erzeugt eine Zip-Datei aus dem übergebenen Dateiname. Es wird einfach die
	 * Extension .zip an den Dateinamen angehagen.
	 * 
	 * @param dateinamen der Dateiname der verwendet wird
	 * @throws IOException bei Fehlern.
	 */
	public static void erzeugeZip(String dateinamen) throws IOException {

		LOG.debug("Erzeuge neue Archive ZIP Datei: " + dateinamen + DATEI_EXTENSION_ZIP);

		File fileToZip = new File(dateinamen);

		try (FileOutputStream fos = new FileOutputStream(dateinamen + DATEI_EXTENSION_ZIP);
				ZipOutputStream zipOut = new ZipOutputStream(fos);
				FileInputStream fis = new FileInputStream(fileToZip);) {

			ZipEntry zipEntry = new ZipEntry(fileToZip.getName());
			zipOut.putNextEntry(zipEntry);
			byte[] bytes = new byte[1024];
			int length;
			while ((length = fis.read(bytes)) >= 0) {
				zipOut.write(bytes, 0, length);
			}
		}
	}
}