package de.wenzlaff.bibelleseplan;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.itextpdf.io.font.constants.StandardFonts;
import com.itextpdf.kernel.events.PdfDocumentEvent;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.action.PdfAction;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;

import de.wenzlaff.twbibel.JwOrg;

/**
 * PDF Druck mit iText 7.
 * 
 * @author Thomas Wenzlaff
 */
public class Druck {

	private static final Logger LOG = LogManager.getLogger(Druck.class);

	/**
	 * Drucke ein PDF Dokument.
	 * 
	 * @param ganzerPlan der gedruckt werden sollen
	 * @param dateiname  der Dateiname
	 * @throws IOException bei Fehlern
	 */
	public static void printPdfDokument(List<Plan> ganzerPlan, String dateiname) throws IOException {

		LOG.info("Drucke PDF Dokument: " + dateiname + " mit " + ganzerPlan.size() + " Zeilen");

		PdfDocument pdfDoc = new PdfDocument(new PdfWriter(dateiname));
		setDocInfo(pdfDoc);

		HeaderHandler headerHandler = new HeaderHandler("Bibellesplan Seite %d", true);
		pdfDoc.addEventHandler(PdfDocumentEvent.START_PAGE, headerHandler);

		try (Document doc = new Document(pdfDoc, PageSize.A4)) {

			doc.setMargins(50, 30, 30, 100);

			PdfFont timesFont = PdfFontFactory.createFont(StandardFonts.TIMES_ROMAN);

			ganzerPlan.forEach((plan -> {
				Cell zeilenCell = new Cell();
				com.itextpdf.layout.element.Link zeile = new com.itextpdf.layout.element.Link("[  ] " + plan.toString(),
						PdfAction.createURI(getOnlineBibleLink(plan)));

				zeilenCell.add(new Paragraph(zeile).setFont(timesFont).setFontSize(12));
				doc.add(zeilenCell);
			}));

			LOG.info("Anzahl Seiten im PDF: " + pdfDoc.getNumberOfPages());
		}
	}

	private static void setDocInfo(PdfDocument pdfDoc) {
		pdfDoc.getDocumentInfo().setAuthor("Thomas Wenzlaff");
		pdfDoc.getDocumentInfo().setCreator("Thomas Wenzlaff");
		pdfDoc.getDocumentInfo().setTitle("Bibelleseplan");
		pdfDoc.getDocumentInfo().setSubject("Bibelleseplan erzeugt am " + LocalDate.now());
	}

	private static String getOnlineBibleLink(Plan plan) {

		Parameter parameter = new Parameter();
		parameter.setSprache("de");
		parameter.setBibelübersetzung("RBI");
		parameter.setBibelbuch(plan.getBibelbuchName());
		URL link = null;
		try {
			link = JwOrg.getOnlineBibelLink(parameter, Integer.valueOf(plan.getKapitel()));
		} catch (NumberFormatException | MalformedURLException e) {
			LOG.error("Fehler beim Linkerstellen " + e.getLocalizedMessage());
			throw new RuntimeException("URL ungültig. " + e.getLocalizedMessage(), e);
		}
		return link.toString();
	}
}