package de.wenzlaff.bibelleseplan;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.Callable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

/**
 * PDF Bibelleseplan.
 * 
 * @author Thomas Wenzlaff
 */
@Command(name = "PDFPlan", mixinStandardHelpOptions = true, version = "PDFPlan 1.0", description = "Erzeugt einen Bibelleseplan im PDF Format.", showDefaultValues = true, footer = {
		"@|fg(green) Thomas Wenzlaff|@",
		"@|fg(red),bold http://www.wenzlaff.info|@" })
public class PDFPlan implements Callable<Integer> {

	private static final Logger LOG = LogManager
			.getLogger(PDFPlan.class);

	@Option(names = { "-s",
			"--startdatum" }, converter = GermanLocalDateConverter.class, description = "das Startdatum des Bibellese Plan im Format Tag.Monat.Jahr z.B. 30.01.2021", defaultValue = "01.01.2021")
	private static LocalDate startDatum;

	@Option(names = { "-k",
			"--kapitelprotag" }, description = "die Anzahl der Kapitel die pro Tag erzeugt werden", defaultValue = "3")
	private static int kapitelProTag;

	/**
	 * Start Methode für das erzeugen eines PDF Bibleleseplanes mit iText 7.
	 * 
	 * @param args das Startdatum, default: 1.1.2021 und anzahl der Kapitel pro Tag,
	 *             default 3 z.B. -s 01.01.2021 -k 3
	 */
	public static void main(String[] args) {

		int exitCode = new CommandLine(new PDFPlan())
				.execute(args);
		System.exit(exitCode);
	}

	@Override
	public Integer call() throws Exception {

		LOG.info("Starte mit Startdatum: "
				+ PDFPlan.startDatum.format(DateTimeFormatter
						.ofPattern("dd.MM.yyyy"))
				+ " und " + PDFPlan.kapitelProTag
				+ " Kapitel pro Tag");

		List<Plan> p = Generator
				.getBibelleseplan(null, startDatum, kapitelProTag);

		String dateiname = getDateiname(startDatum, kapitelProTag);

		Druck.printPdfDokument(p, dateiname);

		LOG.info("Erzeugte PDF-Datei: " + dateiname);

		return 0;
	}

	private static String getDateiname(LocalDate heute,
			int kapitelProTag) {

		return "bibelleseplan-" + kapitelProTag + "-pro-tag-ab-"
				+ heute.format(DateTimeFormatter
						.ofPattern("dd.MM.yyyy"))
				+ ".pdf";
	}
}