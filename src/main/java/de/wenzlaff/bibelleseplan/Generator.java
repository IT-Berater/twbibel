package de.wenzlaff.bibelleseplan;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.wenzlaff.twbibel.Bibel;
import de.wenzlaff.twbibel.Bibelbuecher;

/**
 * Bibelleseplan Generator um die Bibel in einem Jahr zu lesen.
 * 
 * Default: 3 Kapitel pro Tag und am Wochenende 4 Kapitel pro Tag
 * 
 * @author Thomas Wenzlaff
 *
 */
public class Generator {

	private static final Logger LOG = LogManager.getLogger(Generator.class);

	/** Maximale Tage die benötigt werden. Mind. ein Kapitel pro Tag. */
	private static final int MAXIMALE_TAGE = Bibel.maxKapitel();

	private static int kapitel_counter = 0;
	private static int tag_counter = 0;

	/**
	 * Liefert den Bibelleseplan für ein Buch oder die ganze Bibel (null).
	 * 
	 * @param bibelbuch     oder null für die ganze Bibel
	 * @param startDatum    das Startdatum
	 * @param kapitelProTag Termine/ Kap. pro Tag
	 * @return der Plan zum lesen
	 */
	public static List<Plan> getBibelleseplan(Bibelbuecher bibelbuch, LocalDate startDatum, Integer kapitelProTag) {

		List<Plan> bibellesePlan = new ArrayList<>();

		LocalDate startDate = LocalDate.of(startDatum.getYear(), startDatum.getMonth(), startDatum.getDayOfMonth());

		LocalDate endDate = startDate.plusDays(MAXIMALE_TAGE);

		List<LocalDate> erg = getDatesBetween(startDate, endDate);

		if (bibelbuch == null) {

			Bibel.getAlleKapitel().stream().forEach(buch -> {

				bibellesePlan.add(new Plan(erg.get(tag_counter), buch));

				nextTag(erg, kapitelProTag);
			});
			LOG.info("Liefere Plan für die ganze Bibel.");
		} else {
			Bibel.getAlleKapitel(bibelbuch).stream().forEach(buch -> {

				bibellesePlan.add(new Plan(erg.get(tag_counter), buch));

				nextTag(erg, kapitelProTag);
			});
			LOG.info("Liefere Plan für das Bibelbuch: " + bibelbuch.name());
		}

		LOG.info("Anzahl erzeuger Termine: " + tag_counter + " mit Kapitel pro Tag: " + kapitelProTag);

		tag_counter = 0;
		kapitel_counter = 0;

		return bibellesePlan;
	}

	private static void nextTag(List<LocalDate> erg, Integer kapitelProTag) {

		kapitel_counter++;

		if (isWochenende(erg)) { // TODO: eigenen Parameter für Wochenende
			if (kapitel_counter == kapitelProTag) {
				tag_counter++;
				kapitel_counter = 0;
			}
		} else {
			if (kapitel_counter == kapitelProTag) {
				tag_counter++;
				kapitel_counter = 0;
			}
		}
	}

	private static boolean isWochenende(List<LocalDate> erg) {
		return erg.get(tag_counter).getDayOfWeek().equals(DayOfWeek.SATURDAY) || erg.get(tag_counter).getDayOfWeek().equals(DayOfWeek.SUNDAY);
	}

	/**
	 * Liefert alle Tage zwischen start und ende Datum.
	 * 
	 * Berücksichtigt Schaltjahr!
	 * 
	 * @param startDate
	 * @param endDate
	 * @return alle Datum von bis.
	 */
	private static List<LocalDate> getDatesBetween(LocalDate startDate, LocalDate endDate) {

		long daysBetween = ChronoUnit.DAYS.between(startDate, endDate);

		return IntStream.iterate(0, i -> i + 1).limit(daysBetween).mapToObj(i -> startDate.plusDays(i)).collect(Collectors.toList());
	}

}
