package de.wenzlaff.bibelleseplan;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.wenzlaff.twbibel.BibelUebersetzungen;
import de.wenzlaff.twbibel.Bibelbuecher;
import de.wenzlaff.twbibel.JwOrg;
import de.wenzlaff.twbibel.gui.DateiNamen;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.TimeZone;

/**
 * Die Startklasse für den Bibleleseplan Generator.
 * 
 * @author Thomas Wenzlaff
 *
 */
public class Kommandozeile {

	private static final Logger LOG = LogManager.getLogger(Kommandozeile.class.getName());

	private static final String ANWENDUNG_NAME = "Bibelleseplan-Generator";
	private static final String ANWENDUNG_VERSION = "0.0.4";
	private static final String ANWENDUNG_UND_VERSION = ANWENDUNG_NAME + " " + ANWENDUNG_VERSION;

	/** Die Stunde wo die Termine erstellt werden. */
	private static final int DEFAULT_UHRZEIT_ERINNERUNG = 6;
	/** Die Minuten wo die Termine erstellt werden. */
	private static final int DEFAULT_MINUTEN_ERINNERUNG = 0;

	/**
	 * Die Methode für das Aufrufen der Bibleleseplan Anwendung auf..
	 * 
	 * @param args die Kommandozeilen Parameter
	 * @throws Exception bei Fehler
	 */
	public static void main(String[] args) throws Exception {

		LOG.debug("Starte " + ANWENDUNG_NAME + " ...");

		Parameter parameter = Kommandozeile.parseCommandline(args);
		if (parameter == null) {
			return;
		}
	}

	@SuppressWarnings("static-access")
	public static Parameter parseCommandline(String[] args) throws Exception {
		CommandLineParser parser = new BasicParser();

		Options options = new Options();

		Option help = new Option("h", "hilfe", false, "anzeige der Hilfe und Ende");
		options.addOption(help);

		options.addOption(OptionBuilder.withLongOpt("startdatum")
				.withDescription("das Startdatum (Optional) in der Form JJJJ-MM-TT z.B. 2019-09-01 für den 1.Sep. 2019").hasArg().create("s"));
		options.addOption(OptionBuilder.withLongOpt("bibelbuch")
				.withDescription(
						"das Bibelbuch (Optional). Folgende Namen sind gültig (Groß.- und Kleinschreibung beachten): " + Bibelbuecher.getAlleBibelbuecher())
				.hasArg().create("b"));
		options.addOption(OptionBuilder.withLongOpt("kapitelprotag").withDescription("die Kapitel pro Tag (Optional). Default 3 Termine/Kapitel pro Tag.")
				.hasArg().create("k"));
		options.addOption(OptionBuilder.withLongOpt("ausgabedateiname")
				.withDescription("der Ausgabe Dateiname der erzeugen Kalender Datei (Optional). Default je nach gewählten Optionen.").hasArg().create("a"));
		options.addOption(OptionBuilder.withLongOpt("sprachen")
				.withDescription("die Sprache der Bibelübersetzung (Optional) (bisher möglich de). Default de für Deutsch.").hasArg().create("l"));
		options.addOption(OptionBuilder.withLongOpt("bibeluebersetzung")
				.withDescription("die gewählte Bibelübersetzung (Optional) (bisher möglich (NWT, RBI, BI12). Default NWT").hasArg().create("u"));

		Parameter parameter = new Parameter();

		try {
			CommandLine line = parser.parse(options, args);

			if (line.hasOption("h")) {
				printHelp(options);
				return null;
			}

			// Startdatum oder aktuell
			if (line.getOptionValue("s") == null) {
				parameter.setStartDatum(LocalDate.now().format(DateTimeFormatter.ISO_DATE).toString());
			} else {
				parameter.setStartDatum(line.getOptionValue("s"));
			}

			// Wenn Bibelbuch nicht gesetzt, ganze Bibel

			if (line.hasOption("b")) {
				parameter.setBibelbuch(line.getOptionValue("b"));
			} else {
				// default die ganze Bibel
				parameter.setBibelbuch((String) null);
			}

			// Terminen bzw. Kapitel pro Tag
			if (line.hasOption("k")) {
				parameter.setKapitelProTag(line.getOptionValue("k"));
			} else {
				// default 3 Kapitel und Termine pro Tag
				parameter.setKapitelProTag("3");
			}

			// die Sprache der Bibelübersetzung
			if (line.hasOption("l")) {
				parameter.setSprache(line.getOptionValue("l"));
			} else {
				parameter.setSprache("de"); // geht direkt in den Link! TODO für andere Bibeln
			}

			// die Bibelübersetzung
			if (line.hasOption("u")) {
				parameter.setBibelübersetzung(line.getOptionValue("u"));
			} else {
				parameter.setBibelübersetzung("NWT");
			}

			// der Dateiname, als
			// Achtung: letzen Parameter, da aus denen der Dateiname erzeugt wird
			if (line.hasOption("a")) {
				parameter.setAusgabeDateiName(line.getOptionValue("a"));
			} else {
				parameter.setAusgabeDateiName(DateiNamen.getIcsDateinamen(parameter));
			}

			LOG.info("Starte " + ANWENDUNG_UND_VERSION + " mit Parameter: " + parameter);

			startGenerator(parameter);

		} catch (ParseException exp) {

			LOG.error("Fehler beim parsen der Kommandozeile: " + exp.getMessage());
			printHelp(options);
			return null;
		}
		return parameter;
	}

	private static Calendar startGenerator(Parameter parameter) throws IOException {

		Calendar icsCalendar = Kalender.createKalender();
		TimeZone timezone = Kalender.createTimezoneEuropa();

		Kalender genIcs = new Kalender();

		// null gleich ganze Bibel
		List<Plan> bibelleseplan = Generator.getBibelleseplan(parameter.getBibelbuch(), parameter.getStartDatum(), parameter.getKapitelProTag());

		int terminCount = 0;

		for (Plan plan : bibelleseplan) {

			LocalDateTime eventStartZeitpunkt = plan.getZeitpunkt().atTime(DEFAULT_UHRZEIT_ERINNERUNG, DEFAULT_MINUTEN_ERINNERUNG);

			LOG.debug("Link für:" + parameter.getBibelbuch() + " Buch und Kapitel: " + plan.getBuchUndKapitel() + " Kapitel: " + plan.getKapitel());

			String onlineBibelLink = "";

			if (plan.getBuchUndKapitel() != null && plan.getKapitel() != null && parameter.getBibelbuch() != null) {
				// nur ein Bibelbuch
				BibelUebersetzungen oBibel = new JwOrg(parameter, Integer.valueOf(plan.getKapitel()));
				onlineBibelLink = " Direkter Link zur Online-Bibel: " + oBibel.getOnlineBibelURL().toString();
			} else {
				// ganze Bibel
				// TODO: LINKS ERG.
			}

			genIcs.addTermin(icsCalendar, timezone, eventStartZeitpunkt, "Bibellesen: " + plan.getBuchUndKapitel(),
					"bibelleseplan termin " + (++terminCount) + onlineBibelLink);
		}

		Kalender.writeKalenderInDatei(icsCalendar, parameter.getAusgabeDateiName().getAbsolutePath());

		LOG.info("Export nach Dateinamen: " + parameter.getAusgabeDateiName().getAbsolutePath());
		return icsCalendar;
	}

	private static void printHelp(Options options) {
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp(ANWENDUNG_NAME, options);
	}
}