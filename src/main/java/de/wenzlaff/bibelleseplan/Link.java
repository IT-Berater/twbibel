package de.wenzlaff.bibelleseplan;

import java.net.URISyntaxException;

import org.apache.http.client.utils.URIBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Die Links für den Kontakt.
 * 
 * @author Thomas Wenzlaff
 */
public class Link {

	private static final Logger LOG = LogManager.getLogger(Link.class.getName());

	public URIBuilder getBibelserverComUrl(String ubersetzung, String bibelstelle) {
//		https://www.bibleserver.com/text/{Übersetzung}/{Bibelstelle}
//
//			Beispiel für Johannes 3,16 in der Luther-Übersetzung:
//			https://www.bibleserver.com/text/LUT/Johannes3,16
//
//			Es ist auch möglich, einen Übersetzungsvergleich zu verlinken:
//			https://www.bibleserver.com/text/LUT.ELB.EU/Johannes3,16
		URIBuilder b = null;
		try {
			b = new URIBuilder("https://www.bibleserver.com/text/" + ubersetzung + "/" + bibelstelle);
		} catch (URISyntaxException e) {
			LOG.error("Ungültige URL: " + e.getLocalizedMessage());
		}

		return b;
	}
}
