package de.wenzlaff.bibelleseplan;

import java.io.File;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.wenzlaff.twbibel.Bibelbuecher;

/**
 * Die Programm Parameter für den Kommandozeilen Client.
 * 
 * @author Thomas Wenzlaff
 */
public class Parameter {

	private static final Logger LOG = LogManager.getLogger(Parameter.class.getName());

	private LocalDate startDatum;

	private Bibelbuecher bibelbuch;

	/** Gibt an wieviele Kapitel pro Tag erzeugt werden sollen. */
	private Integer kapitelProTag;

	/** Der Ausgabe Dateiname des Kalenders. */
	private File ausgabeDateiName;

	private String sprache;

	private String bibelübersetzung;

	public LocalDate getStartDatum() {
		return startDatum;
	}

	/**
	 * Setzt das Start Datum oder wenn ungültig dann mit Tagesdatum
	 * 
	 * @param startDatum das Start Datum
	 */
	public void setStartDatum(String startDatum) {
		try {
			this.startDatum = LocalDate.parse(startDatum);
		} catch (DateTimeParseException e) {
			LOG.info("Kann das start Datum nicht parsen, setze das Tagesdatum.");
			this.startDatum = LocalDate.now();
			LOG.info("Termin ab Tagesdatum: " + this.startDatum.format(DateTimeFormatter.ISO_DATE));
		}
	}

	/**
	 * Die Bibelbücher.
	 * 
	 * @return die Bibelbücher.
	 */
	public Bibelbuecher getBibelbuch() {
		return bibelbuch;
	}

	/**
	 * Setzt das Bibelbuch. Null steht für die ganze Bibel.
	 * 
	 * @param bibelbuch oder Null für die ganze Bibel.
	 */
	public void setBibelbuch(String bibelbuch) {
		try {
			if (bibelbuch != null) {
				this.bibelbuch = Bibelbuecher.fixNamenToBibelbuecher(bibelbuch);
			} else {
				this.bibelbuch = null;
			}
		} catch (IllegalArgumentException e) {
			LOG.error("Ungültiges Bibelbuch: " + bibelbuch + " Bei Bibelbüchern ist die Groß.-und Kleinschreibung wichtig. Es sind nur genau diese Bücher gültig: "
					+ Bibelbuecher.getAlleBibelbuecher());
		}
	}

	/**
	 * Setzt die Bibelbücher.
	 * 
	 * @param bibelbuch das Bibelbuch
	 */
	public void setBibelbuch(Bibelbuecher bibelbuch) {
		this.bibelbuch = bibelbuch;
	}

	/**
	 * Wieviele Kapitel pro Tag.
	 * 
	 * @param kapitelProTag
	 */
	public void setKapitelProTag(String kapitelProTag) {
		try {
			this.kapitelProTag = Integer.valueOf(kapitelProTag);
		} catch (NumberFormatException e) {
			this.kapitelProTag = 3;
			LOG.error("Ungültiger Parameter für Kapitel pro Tag: " + kapitelProTag + " Es sind Numerische Wert möglich. Verwende den default 3.");
		}
	}

	/**
	 * Liefert die Kapitel pro Tag
	 * 
	 * @return
	 */
	public Integer getKapitelProTag() {
		return kapitelProTag;
	}

	/**
	 * Setzt die Ausgabe Datei
	 * 
	 * @param ausgabeDateiName
	 */
	public void setAusgabeDateiName(String ausgabeDateiName) {
		this.ausgabeDateiName = new File(ausgabeDateiName);
	}

	/**
	 * Liefert den Ausgabe Datei Name.
	 * 
	 * @return
	 */
	public File getAusgabeDateiName() {
		return ausgabeDateiName;
	}

	public void setSprache(String sprache) {
		this.sprache = sprache;

	}

	public void setBibelübersetzung(String bibelübersetzung) {
		this.bibelübersetzung = bibelübersetzung;

	}

	public String getSprache() {
		return sprache;
	}

	public String getBibelübersetzung() {
		return bibelübersetzung;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Parameter [");
		if (startDatum != null) {
			builder.append("startDatum=");
			builder.append(startDatum);
			builder.append(", ");
		}
		if (bibelbuch != null) {
			builder.append("bibelbuch=");
			builder.append(bibelbuch);
			builder.append(", ");
		}
		if (kapitelProTag != null) {
			builder.append("kapitelProTag=");
			builder.append(kapitelProTag);
			builder.append(", ");
		}
		if (ausgabeDateiName != null) {
			builder.append("ausgabeDateiName=");
			builder.append(ausgabeDateiName);
			builder.append(", ");
		}
		if (sprache != null) {
			builder.append("sprache=");
			builder.append(sprache);
			builder.append(", ");
		}
		if (bibelübersetzung != null) {
			builder.append("bibelübersetzung=");
			builder.append(bibelübersetzung);
		}
		builder.append("]");
		return builder.toString();
	}
}
