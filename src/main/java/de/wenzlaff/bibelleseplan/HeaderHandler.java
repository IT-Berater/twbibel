package de.wenzlaff.bibelleseplan;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.itextpdf.io.font.constants.StandardFonts;
import com.itextpdf.kernel.events.Event;
import com.itextpdf.kernel.events.IEventHandler;
import com.itextpdf.kernel.events.PdfDocumentEvent;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfPage;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;

/**
 * Ein Überschriften Handler mit Seitenzahl und optionale anzeige ob die
 * Überschrift auch auf der ersten Seite angezeigt werden soll oder nicht.
 * 
 * @author Thomas Wenzlaff (www.wenzlaff.info)
 */
public class HeaderHandler implements IEventHandler {

	private static final Logger LOG = LogManager.getLogger(HeaderHandler.class);

	private PdfFont timesRomanFont;

	/**
	 * Flag das anzeigt ob der Headera auch auf der Ersten Seite angezeigt werden
	 * soll.
	 * 
	 * true = auch auf der erster Seite ein Header
	 * 
	 * false = auf der ersten Seite kein Header
	 */
	private boolean headerErsteSeite;

	/**
	 * Zb. "Plan Seite %d" wobei %d für die Nummer steht.
	 */
	private String ueberschrift;

	/**
	 * Mit Header und auf der ersten Seite kein Header.
	 * 
	 * Im Header String kann ein %d für die aktuelle Seitenzahl angegeben werden.
	 * 
	 * @param ueberschrift die Überschrift
	 * @throws IOException bei Fehlern.
	 */
	public HeaderHandler(String ueberschrift) throws IOException {
		this.ueberschrift = ueberschrift;
		timesRomanFont = PdfFontFactory.createFont(StandardFonts.TIMES_ROMAN);
	}

	/**
	 * Konstrukor für den Handler.
	 * 
	 * @param ueberschrift     die Überschrift
	 * @param headerErsteSeite die erste Seite
	 * @throws IOException bei Fehler
	 */
	public HeaderHandler(String ueberschrift, boolean headerErsteSeite) throws IOException {
		this(ueberschrift);
		this.headerErsteSeite = headerErsteSeite;
	}

	@Override
	public void handleEvent(Event event) {
		PdfDocumentEvent docEvent = (PdfDocumentEvent) event;
		PdfDocument pdfDoc = docEvent.getDocument();
		PdfPage page = docEvent.getPage();
		Rectangle pageSize = page.getPageSize();
		int pageNumber = pdfDoc.getPageNumber(page);
		LOG.debug("Überschrift auf Seite " + pageNumber + " ergänzen");
		if (!headerErsteSeite) {
			if (pageNumber == 1) {
				return; // wenn auf der ersten Seite kein Header gewünscht wird
			}
		}
		PdfCanvas pdfCanvas = new PdfCanvas(page.newContentStreamBefore(), page.getResources(), pdfDoc);

		pdfCanvas.beginText().setFontAndSize(timesRomanFont, 9).moveText(pageSize.getWidth() / 2 - 30, pageSize.getTop() - 20).showText(String.format(ueberschrift, pageNumber))
				.endText();
		pdfCanvas.release();
	}
}