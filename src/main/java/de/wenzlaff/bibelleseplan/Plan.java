package de.wenzlaff.bibelleseplan;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.wenzlaff.twbibel.Bibel;

/**
 * Plan.
 * 
 * @author Thomas Wenzlaff
 *
 */
public class Plan {

	private static final Logger LOG = LogManager.getLogger(Plan.class);

	private LocalDate zeitpunkt;

	private String kapitel;

	/**
	 * Konstruktor
	 * 
	 * @param zeitpunkt zeitpunkg
	 * @param kapitel   kapitel
	 */
	public Plan(LocalDate zeitpunkt, String kapitel) {
		LOG.debug("Konstruktor: Zeitpunkt" + zeitpunkt + ", Kapitel: " + kapitel);
		this.zeitpunkt = zeitpunkt;
		this.kapitel = kapitel;
	}

	/**
	 * Liefert den Zeitpunkt.
	 * 
	 * @return der Zeipunkt.
	 */
	public LocalDate getZeitpunkt() {
		return zeitpunkt;
	}

	/**
	 * Liefert das Buch inkl. Kap. z.B.: Jona Kapitel 1
	 * 
	 * @return liefert das Bibelbuch mit Kapitel
	 */
	public String getBuchUndKapitel() {
		return kapitel;
	}

	/**
	 * Liefert nur das Kapitel
	 * 
	 * @return das Kapitel
	 */
	public String getKapitel() {
		String[] k = kapitel.split(Bibel.TRENNER);
		return k[1];
	}

	/**
	 * Liefert nur das Bibelbuch als Namen
	 * 
	 * @return das Bibelbuch
	 */
	public String getBibelbuchName() {
		String[] k = kapitel.split(Bibel.TRENNER);
		return k[0];
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();

		if (zeitpunkt != null) {
			builder.append(zeitpunkt.format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
			builder.append(", ");
		}
		if (kapitel != null) {
			builder.append(kapitel);
		}
		return builder.toString();
	}
}